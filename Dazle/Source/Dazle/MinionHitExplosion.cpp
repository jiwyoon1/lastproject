// Fill out your copyright notice in the Description page of Project Settings.

#include "MinionHitExplosion.h"

// Sets default values
AMinionHitExplosion::AMinionHitExplosion()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	ExplosionHit = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Explosion"));

	ExplosionHit->AttachTo(RootComponent);
	static ConstructorHelpers::FObjectFinder<UParticleSystem> Explo(TEXT("ParticleSystem'/Game/ParagonTwinblast/FX/Particles/Abilities/Nitro/FX/P_TwinBlast_Nitro_HitCharacter.P_TwinBlast_Nitro_HitCharacter'"));
	ExplosionHit->SetTemplate(Explo.Object);
	ExplosionHit->SetRelativeScale3D(FVector(2.0f, 2.0f, 2.0f));

}

// Called when the game starts or when spawned
void AMinionHitExplosion::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AMinionHitExplosion::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

