// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"
#include "BehaviorTree/BTService.h"
#include "BTService_MinionTargetDetect.generated.h"

/**
 * 
 */
UCLASS()
class DAZLE_API UBTService_MinionTargetDetect : public UBTService
{
	GENERATED_BODY()
	public:

		UBTService_MinionTargetDetect();
public:
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
