// Fill out your copyright notice in the Description page of Project Settings.

#include "GranadeBoom.h"

// Sets default values
AGranadeBoom::AGranadeBoom()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RadiForce = CreateDefaultSubobject<URadialForceComponent>(TEXT("RadiForce"));
	ExplosionParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("BoomParticle"));

	RadiForce->SetupAttachment(RootComponent);
	ExplosionParticle->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<UParticleSystem>	pt(TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Explosion.P_Explosion'"));


	if (pt.Succeeded())
		ExplosionParticle->SetTemplate(pt.Object);

	ExplosionParticle->SetRelativeLocation(FVector(0.f,0.f,0.f));
	ExplosionParticle->SetRelativeScale3D(FVector(5.f, 5.f, 5.f));
	
}

// Called when the game starts or when spawned
void AGranadeBoom::BeginPlay()
{
	Super::BeginPlay();

	SetLifeSpan(3);
	RadiForce->FireImpulse();
	ExplosionParticle->Activate(true);
}

// Called every frame
void AGranadeBoom::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


}

