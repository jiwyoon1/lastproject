// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Dazle.h"
#include "Animation/AnimInstance.h"
#include "PlayerAnimInstance.generated.h"

/**
 * 
 */

 
DECLARE_MULTICAST_DELEGATE(FOnAttackHitCheckDelegate)
DECLARE_MULTICAST_DELEGATE(FOnAttackRangeAttack)
/*
enum ANIMATION_TYPE
{
	AT_IDLE,
	AT_RUN,
	AT_ATTACK,
};*/

UCLASS()
class DAZLE_API UPlayerAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
public:
	UPlayerAnimInstance();
private:
	//기본적으로 UPROPERTY로 초기화하면 0이된다.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool bAttack;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool bRangeAttack;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool bIdle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	float characterSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	float characterDirection;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	float fWalkRate;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
	int32	iBaseAnimState;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
	float	fVelocity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool	bJump;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool	attackswich;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool	bDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool	bDead;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool	bDash;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	float fPlayerPitch;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	float fPlayerYaw;

	UAnimMontage*	WalkMontage;
	UAnimMontage*	RangeAttack;
public:
	FOnAttackHitCheckDelegate OnHitCheck;
	FOnAttackRangeAttack OnRangeAttack;
private:
	//ANIMATION_TYPE m_eAnimType;

	UFUNCTION()
	void AnimNotify_Attack_Start();
	UFUNCTION()
	void AnimNotify_Attack_End();
	UFUNCTION()
	void AnimNotify_Hit_End();
	UFUNCTION()
	void AnimNotify_Dead_End();
	UFUNCTION()
	void AnimNotify_Fire_Start();
	UFUNCTION()
	void AnimNotify_Dash_End();
public:
	void SetAttack(bool _bAttack);
	void SetCharacterSpeed(float _characterSpeed);
	void SetCharacterDirection(float _characterDirection);
	void SetWalkRate(float _rate);
	void SetPItchYaw(float newpitch, float newyaw);
	void Hit(bool bhit);
	void Dead();
public:
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;
	void PlayWalkMontage();
	void PlayRangeMontage();
	
};
