// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"

#include"PhysicsEngine/RadialForceComponent.h"
#include "GameFramework/Actor.h"
#include "GranadeBoom.generated.h"

UCLASS()
class DAZLE_API AGranadeBoom : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGranadeBoom();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
public:
	UPROPERTY(EditAnywhere, Category = Particle)
	UParticleSystemComponent*	ExplosionParticle;

	UPROPERTY(EditAnywhere, Category = Boom)
	URadialForceComponent*	RadiForce;
};
