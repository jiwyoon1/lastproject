// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_AttackRange.generated.h"

/**
 * 
 */
UCLASS()
class DAZLE_API UBTTask_AttackRange : public UBTTaskNode
{
	GENERATED_BODY()
public:
	UBTTask_AttackRange();

public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
