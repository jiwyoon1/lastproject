// Fill out your copyright notice in the Description page of Project Settings.

#include "MinionAIController.h"

// Fill out your copyright notice in the Description page of Project Settings.
#include"NavigationSystem.h"
#include"Blueprint/AIBlueprintHelperLibrary.h"
#include"BehaviorTree/BehaviorTree.h"
#include"BehaviorTree/BlackboardData.h"
#include"BehaviorTree/BlackboardComponent.h"
#include"PlayerCharacter.h"
#include"MinionCharacter.h"


const FName AMinionAIController::strOriginPos(TEXT("OriginPos"));
const FName AMinionAIController::strPatrolPos(TEXT("PatrolPos"));
const FName AMinionAIController::strTarget(TEXT("Target"));
const FName AMinionAIController::strAttackRange(TEXT("AttackRange"));

AMinionAIController::AMinionAIController()
{
	fRepeatInterval = 5.f;

	//블랙보드 에셋과 행동트리 에셋을 얻어온다.
	static ConstructorHelpers::FObjectFinder<UBlackboardData> BBObj(TEXT("BlackboardData'/Game/AI/BB_Share.BB_Share'"));
	if (BBObj.Succeeded())
		pBBData = BBObj.Object;

	static ConstructorHelpers::FObjectFinder<UBehaviorTree> BTObj(TEXT("BehaviorTree'/Game/AI/BT_Minion.BT_Minion'"));
	if (BTObj.Succeeded())
		pBTree = BTObj.Object;
}

void AMinionAIController::PostInitializeComponents()
{
	Super::PostInitializeComponents();


}

void AMinionAIController::Possess(APawn * pPawn)
{
	UE_LOG(ARLog, Warning, TEXT("PoDssess"));

	GetWorld()->GetTimerManager().SetTimer(RepeateHandle, this, &AMinionAIController::OnRepeat, fRepeatInterval
		, true);
	//시간을 관리해주는 함수
	//SetTimer내가 원하는 시간마다 호출해주는 함수.

	//Blackboard는 AIController.h에 부모클래스 멤버변수로 잡혀있는
	//BlackboardComponent* 타입의 변수이다.
	//여기에 우리가 만든 블랙보드 데이터를 지정해주어서
	//에디터에서 블랙보드를 편집하면 이 AIController가 사용할 수 있도록 지정해준다.

	if (UseBlackboard(pBBData, Blackboard))
	{
		//SetValue~~변수타입별로 많이 있고 적용가능
		//strOriginPos 이것의 시작 벡터좌표를 저장함
		Blackboard->SetValueAsVector(strOriginPos, pPawn->GetActorLocation());


		auto bPawn = Cast<AMinionCharacter>(pPawn);
		if (bPawn)
		{
			Blackboard->SetValueAsFloat(strAttackRange, 300.f);
			UE_LOG(ARLog, Warning, TEXT("MNMN : %s"), *pPawn->GetName());
		}

		UE_LOG(ARLog, Warning, TEXT("UseBlackboard"));

		if (!RunBehaviorTree(pBTree))
		{
			UE_LOG(ARLog, Warning, TEXT("RunBehaviorTree Failed"));
		}
	}
	//Blackboard는 AI컨트롤러가 컨트롤 하는 블랙보드로 지정을 해버린다.

	Super::Possess(pPawn);

}
//33장E

void AMinionAIController::UnPossess()
{
	UE_LOG(ARLog, Warning, TEXT("UnPossess"));
	Super::UnPossess();
	GetWorld()->GetTimerManager().ClearTimer(RepeateHandle);
}

void AMinionAIController::OnRepeat() //패트롤 위치 정해줌.
{
	UE_LOG(ARLog, Warning, TEXT("AIControll"));

	auto pPawn = GetPawn();

	if (!pPawn)
		return;

	const UNavigationSystemV1* pNavSystem = UNavigationSystemV1::GetNavigationSystem(GetWorld());

	if (!pNavSystem)
	{
		UE_LOG(ARLog, Warning, TEXT("Navigation System Faild"));
		return;
	}

	FNavLocation NextLoc;
	if (pNavSystem->GetRandomPointInNavigableRadius(pPawn->GetActorLocation(), 300.f, NextLoc))
	{
		//UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, NextLoc.Location)d;
		Blackboard->SetValueAsVector(strPatrolPos, NextLoc.Location);
		//UAIBlueprintHelperLibrary::SimpleMoveToActor 엑터를 넣었을때 그 위치로 가는것
		//UAIBlueprintHelperLibrary::SimpleMoveToLocation 좌표를 넣었을떄 그 위치로 가는것.


		UE_LOG(ARLog, Warning, TEXT("X %f  Y%f   Z%f  "), NextLoc.Location.X, NextLoc.Location.Y, NextLoc.Location.Z);
	}
}

const FName AMinionAIController::GetOriginPos()
{
	return strOriginPos;
}

const FName AMinionAIController::GetPatrolPos()
{
	return strPatrolPos;
}

const FName AMinionAIController::GetTargetName()
{
	return strTarget;
}

const FName AMinionAIController::GetAttackRangeName()
{
	return strAttackRange;
}
