// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_MinionPatroll.generated.h"

/**
 * 
 */
UCLASS()
class DAZLE_API UBTTask_MinionPatroll : public UBTTaskNode
{
	GENERATED_BODY()
public:
	UBTTask_MinionPatroll();
public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
