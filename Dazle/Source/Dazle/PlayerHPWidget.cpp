// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerHPWidget.h"
#include"Components/ProgressBar.h"
//프로그레스 바 기능쓰려면 프로그래스 를 불러온다,

//UI시스템이 준비되면 호출되는 함수이다.
void UPlayerHPWidget::NativeConstruct()
{
	
	Super::NativeConstruct();
	UE_LOG(ARLog, Warning, TEXT("NativeConstruct"));

	ProgressBar = Cast<UProgressBar>(GetWidgetFromName(TEXT("test")));
	SetValue(1.f, 1.f);
}

void UPlayerHPWidget::SetValue(float fValue, float fMaxValue)
{
	UE_LOG(ARLog, Warning, TEXT("setvalue"));
	m_fValue = fValue / fMaxValue;
	
}