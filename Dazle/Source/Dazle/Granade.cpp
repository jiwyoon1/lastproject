// Fill out your copyright notice in the Description page of Project Settings.

#include "Granade.h"
#include "GranadeBoom.h"

// Sets default values
AGranade::AGranade()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	mGranadeMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Granade"));

	mGranadeMesh->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<UStaticMesh>	BoxMesh(TEXT("StaticMesh'/Game/Meshes/Box.Box'"));

	if (BoxMesh.Succeeded())
		mGranadeMesh->SetStaticMesh(BoxMesh.Object);

	mGranadeMesh->SetRelativeLocation(FVector(0.f, 0.f, 0.f));
	mGranadeMesh->SetRelativeScale3D(FVector(1.f, 1.f, 1.f));

	/*auto boom = GetWorld()->SpawnActor<AGranadeBoom>(
		GetActorLocation() +FVector(200.f,200.f,200.f),
		FRotator(0.f,0.f,0.f));*/
}

// Called when the game starts or when spawned
void AGranade::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGranade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

