// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_MoveTarget.generated.h"

/**
 * 
 */
UCLASS()
class DAZLE_API UBTTask_MoveTarget : public UBTTaskNode
{
	GENERATED_BODY()
public:
	UBTTask_MoveTarget();
public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
