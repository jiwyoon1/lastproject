// Fill out your copyright notice in the Description page of Project Settings.

#include "BTTask_AttackRange.h"
#include "ShareAIController.h"
#include"BehaviorTree/BlackboardComponent.h"
#include"NavigationSystem.h"
#include"Blueprint/AIBlueprintHelperLibrary.h"
#include"PlayerCharacter.h"

UBTTask_AttackRange::UBTTask_AttackRange()
{
	NodeName = TEXT("RangeAttack");
}

EBTNodeResult::Type UBTTask_AttackRange::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	EBTNodeResult::Type eResult = Super::ExecuteTask(OwnerComp, NodeMemory);

	auto pPawn = Cast<APlayerCharacter>(OwnerComp.GetAIOwner()->GetPawn());

	if (!pPawn)
		return EBTNodeResult::Failed;

	pPawn->AIAttack();


	return EBTNodeResult::InProgress;

}

void UBTTask_AttackRange::TickTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds)
{
	Super::TickTask(OwnerComp, NodeMemory, DeltaSeconds);
}
