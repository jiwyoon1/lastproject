// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"
#include "Blueprint/UserWidget.h"
#include "PlayerHPWidget.generated.h"

/**
 * 
 */
//58장 12분
//멀티케스트 등록할놈은 등록하고 개별적으로 할놈은 범용적처리가 가능하도록 포인터 처리하는게 좋다.
UCLASS()
class DAZLE_API UPlayerHPWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	virtual void NativeConstruct() override;
private:
	UPROPERTY()
	float m_fValue;

	UPROPERTY()
	class UProgressBar* ProgressBar;
public:
	void SetValue(float fValue, float fMaxValue);
	
};
