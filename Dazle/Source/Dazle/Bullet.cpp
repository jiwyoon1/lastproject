// Fill out your copyright notice in the Description page of Project Settings.

#include "Bullet.h"

// Sets default values
ABullet::ABullet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//StaticMesh'/Game/ParagonShinbi/FX/Meshes/Hero_Specific/SM_Daggers_Stalker.SM_Daggers_Stalker'

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	BulletParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particle"));//UParticleSystemComponent 발사체 이동컴포넌트
	MovementCom = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectTileMovement"));
	
	

	SphereCollision->InitSphereRadius(20.f); //충돌체 반지름
	SphereCollision->BodyInstance.SetCollisionProfileName(TEXT("Projectile"));
	SphereCollision->OnComponentHit.AddDynamic(this, &ABullet::OnHit);
	
	RootComponent = SphereCollision;
	

	Mesh->SetupAttachment(SphereCollision);
	BulletParticle->SetupAttachment(SphereCollision);
	MovementCom->SetUpdatedComponent(SphereCollision); // 이 씬컴포넌트가 업데이트되게 해줌.

	 
	//ProjecttileMovement는 탄이 물리적작용을 받기위해서 사용하는 것이다.
	//그냥 직선발사면 일반적인 무브먼트 컴포넌트를 쓰면된다.
	MovementCom->InitialSpeed = 25000.f;//기본스피드
	MovementCom->MaxSpeed = 25000.f;
	MovementCom->bRotationFollowsVelocity = true;//회전
	//tile
	MovementCom->bShouldBounce = true;
	MovementCom->Bounciness = 0.3f;
	
	//static ConstructorHelpers::FObjectFinder<UStaticMesh>	bulletMash(TEXT("StaticMesh'/Game/InfinityBladeGrassLands/Environments/Breakables/StaticMesh/Box/SM_Env_Breakables_Box1.SM_Env_Breakables_Box1'"));

	//if (bulletMash.Succeeded())
	//	Mesh->SetStaticMesh(bulletMash.Object);

	static ConstructorHelpers::FObjectFinder<UParticleSystem>	pt(TEXT("ParticleSystem'/Game/ParagonWraith/FX/Particles/Abilities/ScopedShot/FX/P_Wraith_Sniper_Projectile.P_Wraith_Sniper_Projectile'"));
	//static ConstructorHelpers::FObjectFinder<UParticleSystem>	pt(TEXT("ParticleSystem'/Game/ParagonTwinblast/FX/Particles/Abilities/Ultimate/FX/p_Twinblast_Ult2_MultiTracers.p_Twinblast_Ult2_MultiTracers'"));
	

	if (pt.Succeeded())
		BulletParticle->SetTemplate(pt.Object);
	BulletParticle->SetRelativeScale3D(FVector(0.4f,1.0f,1.0f));
	Mesh->SetCollisionProfileName(TEXT("NoCollision"));
	//InitialLifeSpan = 1.f;
	lifeDistance = 30000.f;
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//MovementCom->Velocity.Distance()
	//lletParticle->SetVisibility(false);

	lifeDistance -= MovementCom->InitialSpeed * DeltaTime;


	if (lifeDistance <= 0.f)
		Destroy();

}

void ABullet::SetDir(const FVector & vDir)
{
	MovementCom->Velocity = vDir * MovementCom->InitialSpeed;
}

void ABullet::SetSelfController(AController  *EventInstigator)
{
	selfController = EventInstigator;
}

void ABullet::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComponent, FVector vNormalImpulse,
	const FHitResult& Hit)
{
	UE_LOG(ARLog, Warning, TEXT("OnHiiiiit!!"));

	if (Hit.Actor.IsValid())
	{
		UE_LOG(ARLog, Warning, TEXT("CollisionCube : %s"), *Hit.Actor->GetName());
		FDamageEvent DamageEvent;
		Hit.Actor->TakeDamage(200.f, DamageEvent, selfController, this);
		Destroy();
		
	}
	if (OtherActor != this && OtherComponent->IsSimulatingPhysics())
	{
		OtherComponent->AddImpulseAtLocation(MovementCom->Velocity * 100.f,
			Hit.ImpactPoint);

		Destroy();
	}
}
