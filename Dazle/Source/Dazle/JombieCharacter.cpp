// Fill out your copyright notice in the Description page of Project Settings.

#include "JombieCharacter.h"
#include"JombieController.h"
#include"DrawDebugHelpers.h"
#include "JombieAnimInstance.h"

// Sets default values
AJombieCharacter::AJombieCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraArm"));

	SpringArm->SetupAttachment(RootComponent); //
	SpringArm->SetRelativeLocation(FVector(0.f, 0.f, 1.f));


	GetCapsuleComponent()->SetCollisionProfileName(TEXT("Player"));

	GetCapsuleComponent()->SetCapsuleHalfHeight(80.f);
	GetCapsuleComponent()->SetCapsuleRadius(30.f);

	GetMesh()->SetRelativeLocationAndRotation(FVector(0.f, 0.f, -88.f), FRotator(0.f, -90.f, 0.f));



	GetCharacterMovement()->bOrientRotationToMovement = true;
	//GetCharacterMovement()->JumpZVelocity = 600.f;

	fAttackRange = 300.f;
	fAttackSize = FVector(100.f, 100.f, 100.f);


	static ConstructorHelpers::FObjectFinder<USkeletalMesh>	bBody(TEXT("SkeletalMesh'/Game/Enemy/manne/manne.manne'"));

	if (bBody.Succeeded())
		GetMesh()->SetSkeletalMesh(bBody.Object);


	static ConstructorHelpers::FClassFinder<UAnimInstance>	PlayerBluePrint(TEXT("AnimBlueprint'/Game/minion/BluePrint/JombieBP.JombieBP_C'"));

	if (PlayerBluePrint.Succeeded())
		GetMesh()->SetAnimInstanceClass(PlayerBluePrint.Class);

	m_pAnimInstance = nullptr;

	bInputAttack = false;

	iHP = 500;
	iHPMax = 500;
	iDead = false;
	fBaseDmage = 100;
	//31장 30분
	AIControllerClass = AJombieController::StaticClass();
	//EAutoPossessAI는 이넘인데 들어가면 4가지가 있는데 
	//PlacedInWorldOrSpawned는 필드에 존재하거나 스폰을 했을경우 AI컨트롤러가 빙의할수있도록 처리하는 것이다.
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	bUseControllerRotationYaw = false;

	obtype = OB_MONSTER;
	bCanBeDamaged = true;
}

// Called when the game starts or when spawned
void AJombieCharacter::BeginPlay()
{
	Super::BeginPlay();

}


// Called every frame
void AJombieCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AJombieCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

float AJombieCharacter::TakeDamage(float DamageAmount, FDamageEvent const & DamageEvent, AController * EventInstigator, AActor * DamageCauser)
{
	float fComputeDamage = Super::TakeDamage(DamageAmount, DamageEvent,
		EventInstigator, DamageCauser);

	iHP -= (int32)fComputeDamage;

	UE_LOG(ARLog, Warning, TEXT("Damage:%f , Hp:%d , ComputeDamaged : %f "), DamageAmount, iHP, fComputeDamage);
	m_pAnimInstance->Hit(true);

	if (iHP <= 0)
	{
		iHP = 0;
		iDead = true;
		m_pAnimInstance->Hit(false);
		m_pAnimInstance->Dead();
		SetActorEnableCollision(false);
	}

	return fComputeDamage;
}

void AJombieCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	m_pAnimInstance = Cast<UJombieAnimInstance>(GetMesh()->GetAnimInstance());

	m_pAnimInstance->OnHitCheck.AddUObject(this, &AJombieCharacter::OnAttack);

}

void AJombieCharacter::PossessedBy(AController * NewController)
{
	Super::PossessedBy(NewController);

	GetCharacterMovement()->MaxWalkSpeed = 300.f;
}


void AJombieCharacter::SetbInputAttack(float _bInputAttack)
{
	bInputAttack = _bInputAttack;
}

bool AJombieCharacter::GetbInputAttack() const
{
	return bInputAttack;
}

bool AJombieCharacter::GetJumpStart() const
{
	return false;
}

int32 AJombieCharacter::GetObType() const
{
	return obtype;
}

void AJombieCharacter::AttackBase()
{
}

void AJombieCharacter::Jumpup()
{
}

void AJombieCharacter::OnAttack()
{


	FCollisionQueryParams Params(NAME_None, false, this);

	TArray<FHitResult>	HitResultArr;

	bool bResult = GetWorld()->SweepMultiByChannel(HitResultArr, GetActorLocation(),
		GetActorLocation() + GetActorForwardVector()*fAttackRange, FQuat::Identity, ECollisionChannel::ECC_GameTraceChannel2,
		FCollisionShape::MakeBox(fAttackSize), Params);

	if (bResult)
	{
		for (auto HitResult : HitResultArr)
		{
			if (HitResult.Actor.IsValid())
			{
				UE_LOG(ARLog, Warning, TEXT("CollisionCube : %s"), *HitResult.Actor->GetName());
				FDamageEvent DamageEvent;
				HitResult.Actor->TakeDamage(fBaseDmage, DamageEvent, GetController(), this);
			}
		}
	}
	//#이붙어있으니 전처리기
	//디버그랑 릴리즈랑 체크해주는게 있음.
	//ENABLE_DRAW_DEBUG 디버그 활성화가 되어있는지 체크해줌.
#if ENABLE_DRAW_DEBUG
	FVector vTrace = GetActorForwardVector()*fAttackRange;//방향과 거리를 나타냄
	FVector vCenter = GetActorLocation() + vTrace * 0.5f;//가운데니까 절반의 거리
	float fHalfHeight = fAttackRange * 0.5f + 0.f;
	FQuat CapsuleRot = FRotationMatrix::MakeFromZ(vTrace).ToQuat();
	FColor DrawColor = !bResult ? FColor::Green : FColor::Red;
	float fDebufLifeTime = 2.f;

	//어그로 범위  라고 써있지만 사실 공격 큐브임
	DrawDebugBox(GetWorld(), vCenter, fAttackSize, CapsuleRot, DrawColor, false, fDebufLifeTime);

	//DrawDebugCapule()
#endif
}

void AJombieCharacter::AttackNear()
{
}

void AJombieCharacter::AIAttack()
{
}
