// Fill out your copyright notice in the Description page of Project Settings.

#include "BTDecorater_AttackCheck.h"
#include"ShareAIController.h"
#include"PlayerCharacter.h"
#include"MinionCharacter.h"
#include"BehaviorTree/BlackboardComponent.h"

UBTDecorater_AttackCheck::UBTDecorater_AttackCheck()
{
	NodeName = TEXT("Attack_Check");
}

bool UBTDecorater_AttackCheck::CalculateRawConditionValue(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory) const
{




	bool bResult=Super::CalculateRawConditionValue(OwnerComp, NodeMemory);
	//UE_LOG(ARLog, Warning, TEXT("ggggggggggggggggggg"));
	auto pPawn = Cast<APlayerCharacter>(OwnerComp.GetAIOwner()->GetPawn());
	if (!pPawn)
	{
		return false;
	}

	auto pTarget = Cast<APlayerCharacter>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AShareAIController::GetTargetName()));

	if (!pTarget)
		return false;

	float fAttackRange = OwnerComp.GetBlackboardComponent()->GetValueAsFloat(AShareAIController::GetAttackRangeName());

	//
	if (pTarget->GetDistanceTo(pPawn) <= fAttackRange)
	{
		UE_LOG(ARLog, Warning, TEXT("pTarget->GetDistanceTo(pPawn) : %f ,  fAttackRange : %f"), pTarget->GetDistanceTo(pPawn), fAttackRange);
		//pPawn->SetbInputAttack(true);
		return true;
	}
	return false;
}
