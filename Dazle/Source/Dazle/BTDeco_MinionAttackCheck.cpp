// Fill out your copyright notice in the Description page of Project Settings.

#include "BTDeco_MinionAttackCheck.h"
#include "MinionAIController.h"
#include"PlayerCharacter.h"
#include"MinionCharacter.h"
#include"BehaviorTree/BlackboardComponent.h"

UBTDeco_MinionAttackCheck::UBTDeco_MinionAttackCheck()
{
	NodeName = TEXT("Minion_Attack_Check");
}

bool UBTDeco_MinionAttackCheck::CalculateRawConditionValue(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory) const
{
	bool bResult = Super::CalculateRawConditionValue(OwnerComp, NodeMemory);

	auto pPawn = Cast<AMinionCharacter>(OwnerComp.GetAIOwner()->GetPawn());
	if (!pPawn)
	{
		return false;
	}

	auto pTarget = Cast<APlayerCharacter>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AMinionAIController::GetTargetName()));

	if (!pTarget)
		return false;

	float fAttackRange = OwnerComp.GetBlackboardComponent()->GetValueAsFloat(AMinionAIController::GetAttackRangeName());

	//
	//UE_LOG(ARLog, Warning, TEXT("Minion : %f, fAtttt : %f"), pTarget->GetDistanceTo(pPawn), fAttackRange);
	if (pTarget->GetDistanceTo(pPawn) <= fAttackRange)
	{
		UE_LOG(ARLog, Warning, TEXT("ggggggggggggggggggg"));
		pPawn->SetbInputAttack(true);
		return true;
	}
	return false;
}
