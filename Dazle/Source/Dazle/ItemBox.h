// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"
#include "GameFramework/Actor.h"
#include "ItemBox.generated.h"

UCLASS()
class DAZLE_API AItemBox : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItemBox();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void PostInitializeComponents() override;
public:
	UPROPERTY(EditAnywhere, Category = Item)
	UBoxComponent*	TriggerBox;

	UPROPERTY(EditAnywhere, Category = Item)
	UStaticMeshComponent*	ItemBox;

	UPROPERTY(EditAnywhere, Category = Item)
	UParticleSystemComponent*	BoxParticle;

private:
	UFUNCTION()
	void OnCharacterOverlap(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor, UPrimitiveComponent *OtherComp,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
};
