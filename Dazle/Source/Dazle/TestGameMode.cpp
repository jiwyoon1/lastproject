// Fill out your copyright notice in the Description page of Project Settings.

#include "TestGameMode.h"
#include"Dazle.h"
#include"PlayerCharacter.h"
#include"MainPlayerController.h"
#include"IngameWidget.h"

ATestGameMode::ATestGameMode()
{
	DefaultPawnClass = APlayerCharacter::StaticClass();
	PlayerControllerClass = AMainPlayerController::StaticClass();
	UE_LOG(ARLog, Warning, TEXT("Game Mode Create "));

	static ConstructorHelpers::FClassFinder<UIngameWidget> HUD(TEXT("WidgetBlueprint'/Game/UI/Ingame.Ingame_C'"));
	if (HUD.Succeeded())
	{
		inGameWidgetClass = HUD.Class;

	}


}

ATestGameMode::~ATestGameMode()
{

}



void ATestGameMode::BeginPlay()
{
	Super::BeginPlay();

	inGameWidget = CreateWidget<UUserWidget>(GetWorld(), inGameWidgetClass);
	//최종적으로 화면에 보여주는 뷰포트에 출력해준다.
	if(inGameWidget)
		inGameWidget->AddToViewport();


}

