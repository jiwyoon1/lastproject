// Fill out your copyright notice in the Description page of Project Settings.

#include "ItemBox.h"
#include"PlayerCharacter.h"
#include"MinionCharacter.h"

// Sets default values
AItemBox::AItemBox()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TriggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("TRIGGER"));
	ItemBox = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ItemBox"));
	//BoxParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particle"));

	RootComponent = TriggerBox;
	ItemBox->SetupAttachment(TriggerBox);
	//BoxParticle->SetupAttachment(TriggerBox);

	TriggerBox->SetBoxExtent(FVector(27.f, 27.5f, 14.f));
	TriggerBox->SetRelativeLocation(FVector(0.f, 0.f, 14.f));

	TriggerBox->SetSimulatePhysics(true);

	static ConstructorHelpers::FObjectFinder<UStaticMesh>	BoxMesh(TEXT("StaticMesh'/Game/Meshes/Box.Box'"));

	if (BoxMesh.Succeeded())
		ItemBox->SetStaticMesh(BoxMesh.Object);

	ItemBox->SetRelativeLocation(FVector(0.f, 0.f, -14.f));
	ItemBox->SetRelativeScale3D(FVector(0.3f,0.3f,0.3f));

	TriggerBox->SetCollisionProfileName(TEXT("Item"));
	ItemBox->SetCollisionProfileName(TEXT("NoCollision"));
}

// Called when the game starts or when spawned
void AItemBox::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AItemBox::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);



}

void AItemBox::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	TriggerBox->OnComponentBeginOverlap.AddDynamic(this,&AItemBox::OnCharacterOverlap);
}

void AItemBox::OnCharacterOverlap(UPrimitiveComponent *OverlappedComponent, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	UE_LOG(ARLog, Warning, TEXT("bos overlap "));

	auto Player = Cast<APlayerCharacter>(OtherActor);
	if (!Player)
	{
		Destroy();
		return;
	}
	Player->SetItemSlotFirst(1);

	Destroy();
	return;

}

