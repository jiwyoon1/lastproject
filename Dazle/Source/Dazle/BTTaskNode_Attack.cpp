#include "BTTaskNode_Attack.h"
#include "ShareAIController.h"
#include"BehaviorTree/BlackboardComponent.h"
#include"NavigationSystem.h"
#include"Blueprint/AIBlueprintHelperLibrary.h"
#include"PlayerCharacter.h"

UBTTaskNode_Attack::UBTTaskNode_Attack()
{
	NodeName = TEXT("Attack");
	bNotifyTick = true;
	//틱을 호출하길 원한다면 bNotifyTick = true; 해주면된다.
	bBTDA_Attack = false;
}

EBTNodeResult::Type UBTTaskNode_Attack::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	EBTNodeResult::Type eResult = Super::ExecuteTask(OwnerComp, NodeMemory);

	auto pPawn = Cast<APlayerCharacter>(OwnerComp.GetAIOwner()->GetPawn());

	if (!pPawn)
		return EBTNodeResult::Failed;

	pPawn->AIAttack();

	bBTDA_Attack = true;

	return EBTNodeResult::InProgress;
}

void UBTTaskNode_Attack::TickTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds)
{
	Super::TickTask(OwnerComp, NodeMemory, DeltaSeconds);
	FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
}
