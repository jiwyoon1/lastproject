// Fill out your copyright notice in the Description page of Project Settings.

#include "BTService_JombieDetect.h"
#include "JombieController.h"
#include"PlayerCharacter.h"
#include"JombieCharacter.h"
#include"BehaviorTree/BlackboardComponent.h"
#include"DrawDebugHelpers.h" //구출력용

UBTService_JombieDetect::UBTService_JombieDetect()
{
	NodeName = TEXT("DeTect_byJombie");
	Interval = 0.1f;
}

void UBTService_JombieDetect::TickNode(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	auto pPawn = OwnerComp.GetAIOwner()->GetPawn();

	if (!pPawn)
		return;

	UWorld* pWorld = pPawn->GetWorld();



	FVector vCenter = pPawn->GetActorLocation();
	float fRadius = 1000.f;
	FCollisionQueryParams Params(NAME_None, false, pPawn);

	//멀티바이채널을 이용해서 여러개가 들어오게한다.
	TArray<FOverlapResult> OverlapResultArr;
	bool bResult = pWorld->OverlapMultiByChannel(OverlapResultArr, vCenter, FQuat::Identity, ECollisionChannel::ECC_GameTraceChannel2,
		FCollisionShape::MakeSphere(fRadius),
		Params);
	//
	if (bResult)
	{

		for (auto OverlapResult : OverlapResultArr)
		{
			APlayerCharacter* pCharacter = Cast<APlayerCharacter>(OverlapResult.GetActor()); //어떤 타입이 오버랩될지모르니 APawn으로 받아준다.

			if (pCharacter && pCharacter->GetController()->IsPlayerController())//&& pCharacter->GetController()->IsPlayerController() //Isplayer로 컨트롤을 구분한다.
			{
				OwnerComp.GetBlackboardComponent()->SetValueAsObject(AJombieController::GetTargetName(), pCharacter);
				/* 어그로 범위
#if ENABLE_DRAW_DEBUG
				DrawDebugSphere(pWorld, vCenter, fRadius, 20, FColor::Red,
					false, 1.f);

				DrawDebugPoint(pWorld, pCharacter->GetActorLocation(), 10, FColor::Green,
					false, 3.f);

				DrawDebugLine(pWorld, pPawn->GetActorLocation(), pCharacter->GetActorLocation(), FColor::Green,
					false, 3.f);

#endif
*/
				return;
			}
		}

		/*
		for (auto OverlapResult : OverlapResultArr)
		{
			UE_LOG(ARLog, Warning, TEXT("minon detect"));
			AMinionCharacter* pCharacter = Cast<AMinionCharacter>(OverlapResult.GetActor()); //어떤 타입이 오버랩될지모르니 APawn으로 받아준다.

			if (pCharacter)//&& pCharacter->GetController()->IsPlayerController() //Isplayer로 컨트롤을 구분한다.
			{
				OwnerComp.GetBlackboardComponent()->SetValueAsObject(AShareAIController::GetTargetName(), pCharacter);

#if ENABLE_DRAW_DEBUG
				DrawDebugSphere(pWorld, vCenter, fRadius, 20, FColor::Red,
					false, 1.f);

				DrawDebugPoint(pWorld, pCharacter->GetActorLocation(), 10, FColor::Green,
					false, 3.f);

				DrawDebugLine(pWorld, pPawn->GetActorLocation(), pCharacter->GetActorLocation(), FColor::Green,
					false, 3.f);
				return;
#endif

			}
		}*/

	}

	//UE_LOG(ARLog, Warning, TEXT("I dont Find you"));
	OwnerComp.GetBlackboardComponent()->SetValueAsObject(AJombieController::GetTargetName(), nullptr);
	/* 어그로 범위
#if ENABLE_DRAW_DEBUG
	DrawDebugSphere(pWorld, vCenter, fRadius, 20, FColor::Green,
		false, 1.f);



# endif*/
}

