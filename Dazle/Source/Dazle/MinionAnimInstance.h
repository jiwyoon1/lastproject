// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"
#include "Animation/AnimInstance.h"
#include "MinionAnimInstance.generated.h"

/**
 * 
 */



DECLARE_MULTICAST_DELEGATE(FOnAttackHitCheckDelegate)

/*
enum ANIMATION_TYPE
{
	AT_IDLE,
	AT_RUN,
	AT_ATTACK,
};*/




UCLASS()
class DAZLE_API UMinionAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
public:
	UMinionAnimInstance();
private:
		//기본적으로 UPROPERTY로 초기화하면 0이된다.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool bAttack;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool bIdle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	float characterSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	float characterDirection;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
	int32	iBaseAnimState;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
	float	fVelocity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool	bJump;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool	attackswich;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool	bDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, Meta = (AllowPrivateAccess = true))
	bool	bDead;

	UAnimMontage*	AttackMontage;
public:
	FOnAttackHitCheckDelegate OnHitCheck;

private:
	//ANIMATION_TYPE m_eAnimType;
	
	UFUNCTION()
	void AnimNotify_Attack_Start();
	UFUNCTION()
	void AnimNotify_Attack_End();
	UFUNCTION()
	void AnimNotify_Hit_End();
	UFUNCTION()
	void AnimNotify_Dead_End();
public:
	void SetAttack(bool _bAttack);
	void SetCharacterSpeed(bool _characterSpeed);
	void SetCharacterDirection(bool _characterDirection);
	void Hit(bool bhit);
	void Dead();
public:
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;
	void PlayAttack();
};
