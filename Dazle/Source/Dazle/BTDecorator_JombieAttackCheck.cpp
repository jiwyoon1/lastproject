// Fill out your copyright notice in the Description page of Project Settings.

#include "BTDecorator_JombieAttackCheck.h"
#include "JombieController.h"
#include"PlayerCharacter.h"
#include"JombieCharacter.h"
#include"BehaviorTree/BlackboardComponent.h"

UBTDecorator_JombieAttackCheck::UBTDecorator_JombieAttackCheck()
{
	NodeName = TEXT("Jombie_Attack_Check");
}

bool UBTDecorator_JombieAttackCheck::CalculateRawConditionValue(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory) const
{
	bool bResult = Super::CalculateRawConditionValue(OwnerComp, NodeMemory);

	auto pPawn = Cast<AJombieCharacter>(OwnerComp.GetAIOwner()->GetPawn());
	if (!pPawn)
	{
		return false;
	}

	auto pTarget = Cast<APlayerCharacter>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AJombieController::GetTargetName()));

	if (!pTarget)
		return false;

	float fAttackRange = OwnerComp.GetBlackboardComponent()->GetValueAsFloat(AJombieController::GetAttackRangeName());

	//
	//UE_LOG(ARLog, Warning, TEXT("Zombie : %f, fAtttt : %f"), pTarget->GetDistanceTo(pPawn), fAttackRange);
	if (pTarget->GetDistanceTo(pPawn) <= fAttackRange)
	{
		//UE_LOG(ARLog, Warning, TEXT("ggggggggggggggggggg"));
		pPawn->SetbInputAttack(true);
		return true;
	}
	return false;
}
