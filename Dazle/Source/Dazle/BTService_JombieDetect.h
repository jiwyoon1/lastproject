// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"
#include "BehaviorTree/BTService.h"
#include "BTService_JombieDetect.generated.h"

/**
 * 
 */
UCLASS()
class DAZLE_API UBTService_JombieDetect : public UBTService
{
	GENERATED_BODY()
public:

	UBTService_JombieDetect();
public:
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
