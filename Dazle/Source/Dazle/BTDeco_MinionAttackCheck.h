// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"
#include "BehaviorTree/BTDecorator.h"
#include "BTDeco_MinionAttackCheck.generated.h"

/**
 * 
 */
UCLASS()
class DAZLE_API UBTDeco_MinionAttackCheck : public UBTDecorator
{
	GENERATED_BODY()
public:
	UBTDeco_MinionAttackCheck();

public:
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
	
};
