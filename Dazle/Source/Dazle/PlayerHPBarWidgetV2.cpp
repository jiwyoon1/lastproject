// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerHPBarWidgetV2.h"
#include"Components/ProgressBar.h"

//UI시스템이 준비되면 호출되는 함수이다.
void UPlayerHPBarWidgetV2::NativeConstruct()
{

	Super::NativeConstruct();
	UE_LOG(ARLog, Warning, TEXT("NativeConstruct"));

	ProgressBar = Cast<UProgressBar>(GetWidgetFromName(TEXT("ProgressBar_170")));
	SetValue(1.f, 1.f);


}


void UPlayerHPBarWidgetV2::SetValue(float fvalue, float fMaxvalue)
{
	
	m_fValue = fvalue / fMaxvalue;
	UE_LOG(ARLog, Warning, TEXT("setvalue : %f "), m_fValue);
	
	if (ProgressBar)
		ProgressBar->SetPercent(m_fValue);
}
