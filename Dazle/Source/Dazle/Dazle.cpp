// Fill out your copyright notice in the Description page of Project Settings.

#include "Dazle.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Dazle, "Dazle" );
DEFINE_LOG_CATEGORY(ARLog);