// Fill out your copyright notice in the Description page of Project Settings.
#include "PlayerAnimInstance.h"
#include "PlayerCharacter.h"


UPlayerAnimInstance::UPlayerAnimInstance()
{
	bAttack = false;
	bRangeAttack = false;
	bIdle = false;
	//m_eAnimType = AT_IDLE;
	attackswich = false;
	characterSpeed = 250.f;
	fWalkRate = 1.f;

	characterDirection = 0.f;

	static ConstructorHelpers::FObjectFinder<UAnimMontage>	TurnWalk(TEXT("AnimMontage'/Game/Montage/NearingAttack.NearingAttack'"));

	if (TurnWalk.Succeeded())
		WalkMontage = TurnWalk.Object;

	static ConstructorHelpers::FObjectFinder<UAnimMontage>	RangeA(TEXT("AnimMontage'/Game/Montage/RangeAttack.RangeAttack'"));

	if (TurnWalk.Succeeded())
		RangeAttack = RangeA.Object;
	
	bDamage = false;
	bDead = false;
}

void UPlayerAnimInstance::AnimNotify_Attack_Start()
{
	UE_LOG(ARLog, Warning, TEXT("Attack Start")); 
	OnHitCheck.Broadcast();
}

void UPlayerAnimInstance::AnimNotify_Attack_End()
{
	attackswich = false;
}

void UPlayerAnimInstance::AnimNotify_Hit_End()
{
	bDamage = false;
}

void UPlayerAnimInstance::AnimNotify_Dead_End()
{
	APlayerCharacter*	pPlayer = Cast<APlayerCharacter>(TryGetPawnOwner());
	pPlayer->Destroy();
}

void UPlayerAnimInstance::AnimNotify_Fire_Start()
{
	UE_LOG(ARLog, Warning, TEXT("Range Attack Start"));
	OnRangeAttack.Broadcast();
}

void UPlayerAnimInstance::AnimNotify_Dash_End()
{
	//UE_LOG(ARLog, Warning, TEXT("DashEnd"));
	bDash = false;
}

void UPlayerAnimInstance::SetAttack(bool _bAttack)
{
	bAttack = _bAttack;
}

void UPlayerAnimInstance::SetCharacterSpeed(float _characterSpeed)
{
}

void UPlayerAnimInstance::SetCharacterDirection(float _characterDirection)
{
	characterDirection = _characterDirection;
}

void UPlayerAnimInstance::SetWalkRate(float _rate)
{
	fWalkRate = _rate;
}

void UPlayerAnimInstance::SetPItchYaw(float newpitch, float newyaw)
{
	fPlayerPitch = newpitch;
	fPlayerYaw = newyaw;
}

void UPlayerAnimInstance::Hit(bool bhit)
{
	bDamage = bhit;
	UE_LOG(ARLog, Warning, TEXT("Hit"));
}

void UPlayerAnimInstance::Dead()
{
	bDead = true;
}

void UPlayerAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	//이 AnimInstatce를 가지고 있는 pawn을 얻어온다.

	//TryGetPawnOwner()을 AplayerCharacter로 다운캐스팅이 가능하다.
	//이렇게 쓸거면 PlayerAniinstance가 플레이어캐릭터안에 들어가야한다.
	//플레이어에 지정할 전용 클래스를 만들고 있는것이다.

	//얘를 몬스터에 넣어주면 얘를 가지고 있는 폰은 몬스터다 이 코드 짜면 플레이어 캐릭터로 형변환하고있다.
	//다운캐스팅을 할때 클래스 A가 있고 B와 C가 둘다 상속받고있을떄
	//객체 B를 생성하고 A를 담을수 있다.
	//A는 부모의 포인터인데 이것을 B로 다운캐스팅 가능하고 C로도 가능하다.
	//그것을 c++에서 보장하지는 않는다.
	//그래서 아주 주의해야한다.
	//플레이어 캐릭터의 전용 애님인스턴스가 되야하는 이유는 몬스터가 가지고있는데 지금 나는 플레이어 캐릭터를 형변환하고있다.
	//이것은 잘못된 경우다(6:47)
	//폰의 기능을 사용하는건 문제가없지만 PlayerChracter의 기능을 사용하는것이 문제가 된다.
	//auto Pawn = (APlayerCharacter*)TryGetPawnOwner();
	//bAttack = Pawn->GetbInputAttack(); //17파트11분47초

	APlayerCharacter*	pPlayer = Cast<APlayerCharacter>(TryGetPawnOwner());

	if(pPlayer)
	{
		bAttack = pPlayer->GetbInputAttack();
		bRangeAttack = pPlayer->GetbInputRangeAttack();

		if (bAttack == true)
		{
			UE_LOG(ARLog, Warning, TEXT("Attack V"));
			bAttack = false;
			pPlayer->SetbInputAttack(false);
			//attackswich = true;  이제 몽타주를 받기 떄문에 필요가 없게됨.
			PlayWalkMontage();
		}

		if (bRangeAttack==true)
		{

			//UE_LOG(ARLog, Warning, TEXT("Attack Mouse %f"), characterDirection);
			bRangeAttack = false;
			pPlayer->SetbInputRangeAttack(false);
			PlayRangeMontage();
		}


		fVelocity = pPlayer->GetVelocity().Size();
		fVelocity = pPlayer->GetVelocity().Size();
		//if (pPlayer->GetCharacterMovement()->Velocity.Size() == 0)
		if (fVelocity == 0)
		{
			iBaseAnimState = 0;
		}
		else
		{
			iBaseAnimState = 1;
		}

		pPlayer->SetbInputAttack(false);
		//pPlayer->GetVelocity();
		//pPlayer->GetControlRotation(); //이부분
		

		// IsFalling 함수는 액터가 공중에 떠있는지 판단해준다.
		// true는 떠있는 상태 false면 바닥을 밟고있는 상태.
		//bJump = pPlayer->GetCharacterMovement()->IsFalling();
		//if (bAttack)
			//pPlayer->SetbInputAttack(false);
		//17파트부터 시작 대충 흟어보기공부법



		//점프
		if (pPlayer->GetDashCollTime() > 0)
		{
			bDash = true;
			//UE_LOG(ARLog, Warning, TEXT("DashStart"));
		}
		bJump = pPlayer->GetCharacterMovement()->IsFalling();
		

	}

}

void UPlayerAnimInstance::PlayWalkMontage()
{
	if (!Montage_IsPlaying(WalkMontage))
		Montage_Play(WalkMontage, 1.f);
}

void UPlayerAnimInstance::PlayRangeMontage()
{
	if (!Montage_IsPlaying(RangeAttack)) //몽타주가 끝날떄까지 안끝나게 함.
		Montage_Play(RangeAttack, 1.f);
}
