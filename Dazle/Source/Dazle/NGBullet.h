// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"
#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "NGBullet.generated.h"


UCLASS()
class DAZLE_API ANGBullet : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ANGBullet();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
private:

	UPROPERTY(EditAnywhere, Category = Bullet)
	USphereComponent* SphereCollision;

	UPROPERTY(EditAnywhere, Category = Bullet)
	UParticleSystemComponent* BulletParticle;

	UPROPERTY(EditAnywhere, Category = StaticMesh)
	UStaticMeshComponent*		Mesh;

	UPROPERTY(EditAnywhere, Category = Movement)
	UProjectileMovementComponent*	MovementCom;
	//발사체 관련된 프로젝트타일컴포넌트가 있음

	UPROPERTY(EditAnywhere, Category = Controller)
	AController *selfController;
private:
	float lifeDistance;
public:
	void SetDir(const FVector& vDir);
	void SetSelfController(AController  *EventInstigator);
public:
	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComponent, FVector vNormalImpulse,
			const FHitResult& Hit);
};
