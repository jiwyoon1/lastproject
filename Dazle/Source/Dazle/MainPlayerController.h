// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "EngineMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MainPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class DAZLE_API AMainPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	virtual void PostInitializeComponents()	override;
	virtual void Possess(APawn* pPawn)	override;

protected:
	virtual void BeginPlay()	override;
};
