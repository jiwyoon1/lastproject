// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"
#include "BehaviorTree/BTService.h"
#include "BTService_TargetDetect.generated.h"

/**
 * 
 */
UCLASS()
class DAZLE_API UBTService_TargetDetect : public UBTService
{
	GENERATED_BODY()

	UBTService_TargetDetect();
public:
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
