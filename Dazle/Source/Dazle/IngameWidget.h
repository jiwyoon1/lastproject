// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"
#include "Blueprint/UserWidget.h"
#include "IngameWidget.generated.h"

/**
 * 
 */
UCLASS()
class DAZLE_API UIngameWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	virtual void NativeConstruct() override;
public:
	UFUNCTION(BlueprintCallable)
	void EmptyTestBtn();

private:
	UPROPERTY()
	float m_fValue;

	UPROPERTY()
	class UProgressBar* HpBar;
public:
	void SetValue(float fvalue, float fMaxvalue);
};
