// Fill out your copyright notice in the Description page of Project Settings.

#include "BTTask_Patrol.h"
#include "ShareAIController.h"
#include"BehaviorTree/BlackboardComponent.h"
//블랙보드 컴포넌트를 쓰게끔한다.
#include"NavigationSystem.h"
#include"Blueprint/AIBlueprintHelperLibrary.h"

UBTTask_Patrol::UBTTask_Patrol()
{
	NodeName = TEXT("Patrol");
}

EBTNodeResult::Type UBTTask_Patrol::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	EBTNodeResult::Type eResult = Super::ExecuteTask(OwnerComp, NodeMemory);
	//이 Task를 사용하는 ,Pawn을 얻어온다.
	auto pPawn = OwnerComp.GetAIOwner();

	if (!pPawn)
	{
		return EBTNodeResult::Failed;
	}

	//Blackboard로 부터 패트롤을 시작할 위치 정보를 얻어온다.
	//처음 스폰된 위치 좌표를 뽑아온다.
	FVector vOriginPos=OwnerComp.GetBlackboardComponent()->GetValueAsVector(AShareAIController::GetOriginPos());

	//움직일 NextPos를 구한다.
	//당연히 네비를 이용해서 만든다. 

	UNavigationSystemV1* pNavSystem = UNavigationSystemV1::GetNavigationSystem(OwnerComp.GetAIOwner()->GetWorld());

	if (!pNavSystem)
	{
		UE_LOG(ARLog, Warning, TEXT("BT_Navigation System Faild"));
		return EBTNodeResult::Failed;
	}

	FNavLocation NextPos;

	if (pNavSystem->GetRandomPointInNavigableRadius(pPawn->GetPawn()->GetActorLocation(), 3000.f, NextPos))
	{
		//UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, NextLoc.Location);

		OwnerComp.GetBlackboardComponent()->SetValueAsVector(AShareAIController::GetPatrolPos(), NextPos.Location);

		return EBTNodeResult::Succeeded;
	}

	return EBTNodeResult::Failed;
}
