// Fill out your copyright notice in the Description page of Project Settings.

#include "MinionAnimInstance.h"
#include"MinionCharacter.h"

UMinionAnimInstance::UMinionAnimInstance()
{
	bAttack = false;
	bIdle = false;
	//m_eAnimType = AT_IDLE;
	attackswich = false;
	characterSpeed = 50.f;


	characterDirection = 0.f;

	static ConstructorHelpers::FObjectFinder<UAnimMontage>	AttackMon(TEXT("AnimMontage'/Game/minion/Montage/NearAttack1.NearAttack1'"));

	if (AttackMon.Succeeded())
		AttackMontage = AttackMon.Object;

	bDamage = false;
	bDead = false;
}

void UMinionAnimInstance::AnimNotify_Attack_Start()
{
	UE_LOG(ARLog, Warning, TEXT("Minion Attack Start"));
	OnHitCheck.Broadcast();
}

void UMinionAnimInstance::AnimNotify_Attack_End()
{
	attackswich = false;
}

void UMinionAnimInstance::AnimNotify_Hit_End()
{
	bDamage = false;
}

void UMinionAnimInstance::AnimNotify_Dead_End()
{
	AMinionCharacter*	pPlayer = Cast<AMinionCharacter>(TryGetPawnOwner());
	pPlayer->Destroy();
}

void UMinionAnimInstance::SetAttack(bool _bAttack)
{
}

void UMinionAnimInstance::SetCharacterSpeed(bool _characterSpeed)
{
}

void UMinionAnimInstance::SetCharacterDirection(bool _characterDirection)
{
}

void UMinionAnimInstance::Hit(bool bhit)
{
	bDamage = bhit;
}

void UMinionAnimInstance::Dead()
{
	bDead = true;
}

void UMinionAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	//이 AnimInstatce를 가지고 있는 pawn을 얻어온다.

	//TryGetPawnOwner()을 AplayerCharacter로 다운캐스팅이 가능하다.
	//이렇게 쓸거면 PlayerAniinstance가 플레이어캐릭터안에 들어가야한다.
	//플레이어에 지정할 전용 클래스를 만들고 있는것이다.

	//얘를 몬스터에 넣어주면 얘를 가지고 있는 폰은 몬스터다 이 코드 짜면 플레이어 캐릭터로 형변환하고있다.
	//다운캐스팅을 할때 클래스 A가 있고 B와 C가 둘다 상속받고있을떄
	//객체 B를 생성하고 A를 담을수 있다.
	//A는 부모의 포인터인데 이것을 B로 다운캐스팅 가능하고 C로도 가능하다.
	//그것을 c++에서 보장하지는 않는다.
	//그래서 아주 주의해야한다.
	//플레이어 캐릭터의 전용 애님인스턴스가 되야하는 이유는 몬스터가 가지고있는데 지금 나는 플레이어 캐릭터를 형변환하고있다.
	//이것은 잘못된 경우다(6:47)
	//폰의 기능을 사용하는건 문제가없지만 PlayerChracter의 기능을 사용하는것이 문제가 된다.
	//auto Pawn = (APlayerCharacter*)TryGetPawnOwner();
	//bAttack = Pawn->GetbInputAttack(); //17파트11분47초

	AMinionCharacter*	pPlayer = Cast<AMinionCharacter>(TryGetPawnOwner());
	
	
	if (pPlayer)
	{
		
		bAttack = pPlayer->GetbInputAttack();

		if (bAttack == true)
		{
			UE_LOG(ARLog, Warning, TEXT("MinionAttack On"));
			bAttack = false;
			pPlayer->SetbInputAttack(false);
			//attackswich = true;  이제 몽타주를 받기 떄문에 필요가 없게됨.
			PlayAttack();
		}

		fVelocity = pPlayer->GetVelocity().Size();
		//if (pPlayer->GetCharacterMovement()->Velocity.Size() == 0)
		if (fVelocity == 0)
		{
			iBaseAnimState = 0;
		}
		else
		{
			iBaseAnimState = 1;
			
		}

		pPlayer->SetbInputAttack(false);
		//pPlayer->GetVelocity();
		//pPlayer->GetControlRotation(); //이부분


		// IsFalling 함수는 액터가 공중에 떠있는지 판단해준다.
		// true는 떠있는 상태 false면 바닥을 밟고있는 상태.
		//bJump = pPlayer->GetCharacterMovement()->IsFalling();
		//if (bAttack)
			//pPlayer->SetbInputAttack(false);
		//17파트부터 시작 대충 흟어보기공부법



		//점프
		bJump = pPlayer->GetCharacterMovement()->IsFalling();

	}
}

void UMinionAnimInstance::PlayAttack()
{
	if (!Montage_IsPlaying(AttackMontage))
	{
		Montage_Play(AttackMontage, 1.f);
		UE_LOG(ARLog, Warning, TEXT("MinionMon"));
	}
}

