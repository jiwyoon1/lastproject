// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"
#include "AIController.h"
#include "JombieController.generated.h"

/**
 * 
 */
UCLASS()
class DAZLE_API AJombieController : public AAIController
{
	GENERATED_BODY()
public:
	AJombieController();
	virtual void PostInitializeComponents() override;//�ʱ�ȭ�ɋ�
	virtual void Possess(APawn* pPawn) override; //���ǰ� �ɋ� ������ �Լ�.
	virtual void UnPossess() override; //���ǰ� Ǯ���� ������ �Լ�.

private:
	void OnRepeat();
private:
	float fRepeatInterval;//�����ð����� ����ٴϰ� �������.
	FTimerHandle RepeateHandle;//Ÿ�̸��Լ��� ��������� ���ð��� �����ص�.

private:
	//���漱��
	UPROPERTY()
		class UBehaviorTree* pBTree;

	UPROPERTY()
		class UBlackboardData* pBBData;

private:
	//����Ʈ�ѷ��� ������ ��� ���� ���� �޸𸮰����� �����ؼ� ����ϰԵ�.
	const static FName strOriginPos;
	const static FName strPatrolPos;
	const static FName strTarget;
	const static FName strAttackRange;
public:
	static const FName GetOriginPos();
	static const FName GetPatrolPos();
	static const FName GetTargetName();
	const static FName GetAttackRangeName();
};
