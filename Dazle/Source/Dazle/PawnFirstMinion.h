// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"
#include "GameFramework/Pawn.h"
#include "PawnFirstMinion.generated.h"

UCLASS()
class DAZLE_API APawnFirstMinion : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APawnFirstMinion();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
