// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"
#include "Blueprint/UserWidget.h"
#include "PlayerHPBarWidgetV2.generated.h"

/**
 * 
 */
UCLASS()
class DAZLE_API UPlayerHPBarWidgetV2 : public UUserWidget
{
	GENERATED_BODY()
public:
	virtual void NativeConstruct() override;
private:
	UPROPERTY()
		float m_fValue;

	UPROPERTY()
	class UProgressBar* ProgressBar;
public:
	void SetValue(float fvalue, float fMaxvalue);
};
