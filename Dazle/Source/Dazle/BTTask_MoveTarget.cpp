// Fill out your copyright notice in the Description page of Project Settings.

#include "BTTask_MoveTarget.h"
#include "ShareAIController.h"
#include"BehaviorTree/BlackboardComponent.h"
#include"NavigationSystem.h"
#include"Blueprint/AIBlueprintHelperLibrary.h"
#include"PlayerCharacter.h"


UBTTask_MoveTarget::UBTTask_MoveTarget()
{
	NodeName = TEXT("MoveTarget");
}

EBTNodeResult::Type UBTTask_MoveTarget::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{

	EBTNodeResult::Type eResult = Super::ExecuteTask(OwnerComp, NodeMemory);

	//이 Task를 사용하는 ,Pawn을 얻어온다.
	auto pPawn = OwnerComp.GetAIOwner();
	
	
	if (!pPawn)
	{
		return EBTNodeResult::Failed;
	}

	//Blackboard로 부터 패트롤을 시작할 위치 정보를 얻어온다.
	//처음 스폰된 위치 좌표를 뽑아온다.
	FVector vOriginPos = OwnerComp.GetBlackboardComponent()->GetValueAsVector(AShareAIController::GetOriginPos());

	//움직일 NextPos를 구한다.
	//당연히 네비를 이용해서 만든다. 


	auto ba = Cast<APlayerCharacter>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(AShareAIController::GetTargetName()));
	//ba->GetActorLocation().Normalize();
	//FVector xy = FVector(1.f,0.f,0.f );
	//ba->AddMovementInput(xy,2.f);

	//OwnerComp.GetBlackboardComponent()->SetValueAsVector(AShareAIController::GetPatrolPos(),);
	
	//UAIBlueprintHelperLibrary::SimpleMoveToLocation(pPawn, ba->GetActorLocation()); 이거주석풀면됨

	//pPawn->AddActorLocalRotation();
	pPawn->GetPawn()->GetActorLocation();
	FVector bad= ba->GetActorLocation() - pPawn->GetPawn()->GetActorLocation();
	FRotator baad = FRotationMatrix::MakeFromX(bad).Rotator();

	//UE_LOG(ARLog, Warning, TEXT("TARGET :%f, :%f, :%f"),  baad.Pitch, baad.Yaw, baad.Roll);
	//UE_LOG(ARLog, Warning, TEXT("pAWN :%f, :%f, :%f"), pPawn->GetPawn()->GetActorRotation().Pitch,
	//	pPawn->GetPawn()->GetActorRotation().Yaw, pPawn->GetPawn()->GetActorRotation().Roll);

	pPawn->GetPawn()->SetActorRotation(baad);
	(Cast<APlayerCharacter>(pPawn->GetPawn()))->SetbInputRangeAttack(1);
	/*
	UNavigationSystemV1* pNavSystem = UNavigationSystemV1::GetNavigationSystem(OwnerComp.GetAIOwner()->GetWorld());

	if (!pNavSystem)
	{
		UE_LOG(ARLog, Warning, TEXT("BT_Navigation System Faild"));
		return EBTNodeResult::Failed;
	}

	FNavLocation NextPos;

	if (pNavSystem->GetRandomPointInNavigableRadius(pPawn->GetPawn()->GetActorLocation(), 1000.f, NextPos))
	{
		//UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, NextLoc.Location);

		OwnerComp.GetBlackboardComponent()->SetValueAsVector(AShareAIController::GetPatrolPos(), NextPos.Location);

		return EBTNodeResult::Succeeded;

		

	}
	*/
	return EBTNodeResult::Failed;
}
