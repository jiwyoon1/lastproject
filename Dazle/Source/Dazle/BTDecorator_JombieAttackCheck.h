// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"
#include "BehaviorTree/BTDecorator.h"
#include "BTDecorator_JombieAttackCheck.generated.h"

/**
 * 
 */
UCLASS()
class DAZLE_API UBTDecorator_JombieAttackCheck : public UBTDecorator
{
	GENERATED_BODY()
public:
	UBTDecorator_JombieAttackCheck();

public:
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;

};
