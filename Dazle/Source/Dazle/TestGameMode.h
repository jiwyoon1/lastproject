// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"
#include "GameFramework/GameModeBase.h"
#include "TestGameMode.generated.h"

/**
 * 
 */
UCLASS()
class DAZLE_API ATestGameMode : public AGameModeBase
{
	GENERATED_BODY()
	ATestGameMode();
	~ATestGameMode();
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UI")
	class UUserWidget * inGameWidget;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UI")
	TSubclassOf<UUserWidget> inGameWidgetClass;
protected:
	virtual void BeginPlay() override;
};


