// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTaskNode_JombiePatroll.generated.h"

/**
 * 
 */
UCLASS()
class DAZLE_API UBTTaskNode_JombiePatroll : public UBTTaskNode
{
	GENERATED_BODY()
public:
	UBTTaskNode_JombiePatroll();
public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
