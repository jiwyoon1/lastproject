// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"
#include "GameFramework/Actor.h"
#include "ButtonActor.generated.h"

UCLASS()
class DAZLE_API AButtonActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AButtonActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY()
	class UWidgetComponent* ButtonWidgetComponent;

	UPROPERTY()
	class UButtonWidget* ButtonWidget;
};
