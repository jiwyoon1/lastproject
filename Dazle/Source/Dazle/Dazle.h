// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "EngineMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(ARLog, Log, All);

enum OB_TYPE
{
	OB_NULL,
	OB_MONSTER,
	OB_PLAYER,
	OB_OBJECT
};