// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"
#include "BehaviorTree/BTDecorator.h"
#include "BTDecorater_AttackCheck.generated.h"

/**
 * 
 */
UCLASS()
class DAZLE_API UBTDecorater_AttackCheck : public UBTDecorator
{
	GENERATED_BODY()
public:
	UBTDecorater_AttackCheck();

public:
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
};
