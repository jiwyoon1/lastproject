// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Dazle.h"
#include "GameFramework/Character.h"
#include "JombieCharacter.generated.h"
UCLASS()
class DAZLE_API AJombieCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AJombieCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent,
		AController* EventInstigator, AActor* DamageCauser);

	virtual void PostInitializeComponents()	override;//컴포넌트가 다 초기화 되고 들어오는 함수.

	virtual void PossessedBy(AController* NewController)	override; //c++초기화할떄 


public:

	UPROPERTY(EditAnywhere, Category = Camera)
	USpringArmComponent*	SpringArm;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool	bJump;

	bool iDead;
private:
	OB_TYPE obtype;
	bool bInputAttack;
	class UJombieAnimInstance*	m_pAnimInstance;
	float fAttackRange;
	FVector fAttackSize;
	int32 iHP;
	int32 iHPMax;
	float fBaseDmage;
public:
	void SetbInputAttack(float _bInputAttack);
	bool GetbInputAttack() const;
	bool GetJumpStart()	const;//bjump점프얻기
	int32 GetObType() const;
private:
	void AttackBase();
	void Jumpup();
	void OnAttack();
	void AttackNear();
public://AI관련함수
	void AIAttack();
};
