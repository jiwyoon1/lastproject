// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include"Dazle.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"


DECLARE_MULTICAST_DELEGATE(FOnHPChangedDelegate); //위젯 클래스에 함수를 만들고 등록해서 멀티케스트 델리게이트를 콜벡형태로 바꿀수있을것이다.


UCLASS()
class DAZLE_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


public:	
	virtual void PostInitializeComponents()	override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, 
		AController* EventInstigator, AActor* DamageCauser);
	virtual void PossessedBy(AController* NewController)	override;

public:

	UPROPERTY(EditAnywhere, Category = Camera)
	USpringArmComponent*	SpringArm;

	UPROPERTY(EditAnywhere, Category = Camera)
	UCameraComponent*		Camera;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool	bJump;

	UPROPERTY(VisibleAnywhere, Category = UI)
	class UWidgetComponent* HPBarWidgetComponent;

	UPROPERTY()
	class UPlayerHPBarWidgetV2* HPBarWidget;
	bool iDead;

	UPROPERTY(EditAnywhere, Category = Particle)
	UParticleSystemComponent* DashFIreL;

	//파티클문제
	UPROPERTY(EditAnywhere, Category = Particle)
	UParticleSystemComponent* DashFIreR;

	UPROPERTY(EditAnywhere, Category = Particle)
	UParticleSystemComponent* DashLight;


	UPROPERTY(EditAnywhere, Category = Light)
	USpotLightComponent* HandLight;

	UPROPERTY(EditAnywhere, Category = Light)
	USpotLightComponent* HeadLight;


public:
	//UI 

	UFUNCTION(BlueprintPure, Category = "Health")
	float getPercentHp() const;

	UFUNCTION(BlueprintPure, Category = "Item")
	int32 GetItemSlotFirst() const;

	UFUNCTION(BlueprintPure, Category = "Item")
	int32 GetItemSlotSecond() const;

	UFUNCTION(BlueprintPure,  Category = "Skill")
	float GetDashCollTime() const;

private:
	OB_TYPE obtype;
	bool bInputAttack;
	bool bInputRangeAttack;//원거리공격 스위치
	class UPlayerAnimInstance*	m_pAnimInstance;
	float fAttackRange;
	FVector fAttackSize;
	int32 iHP;
	int32 iHPMax;
	float fBaseDmage;
	float fHPPercent;
	float fCharacterDirection;

	//CHRACTER_TURN eTurnRight;
	//CHRACTER_TURN eTurnFront;


	int32 iItemSlotFirst;
	int32 iItemSlotSecond;
	float fDashCollTime;

public:
	void SetbInputAttack(float _bInputAttack);
	bool GetbInputAttack() const;
	void SetbInputRangeAttack(float _bInputAttack);
	bool GetbInputRangeAttack() const;
	bool GetJumpStart()	const;//bjump점프얻기
	int32 GetObType() const; //필
	void SetHP(float NewHP);
	

	void SetItemSlotFirst(int32 newtem);
	void SetItemSlotSecond(int32 newtem);

	FOnHPChangedDelegate OnHPChanged;
private:
	void SetControlMode(int32 ControlMode);
	void LookUp(float NewAxisValue);//카메라시선
	void Turn(float NewAxisValue);//카메라시선
	void UpDown(float NewAxisValue);
	void LeftRight(float NewAxisValue);
	void OnAttackRange();
	void Jumpup();
	void OnAttack();
	void AttackNear();
	void AttackRange(float NewAxisValue);
	void Zumupdown();
	void Dash();

	void UseFirstItem();
	void UseSecondItem();
public://AI관련함수
	void AIAttack();
	
public:
	template<typename T> //58장 8분대 참조
	void AddHPChangeFunction(T* pObj, void (T::*pFunc)())
	{
		OnHPChanged.AddUObject(pObj, pFunc);
	}
};
