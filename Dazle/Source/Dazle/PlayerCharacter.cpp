// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerCharacter.h"
#include "PlayerAnimInstance.h"
#include"DrawDebugHelpers.h"
#include"ShareAIController.h"
#include"Bullet.h"
#include"Components/WidgetComponent.h"
#include "PlayerHPBarWidgetV2.h"
#include"TestGameMode.h"
#include"UnrealMath.h"
#include"ItemBox.h"
#include"Classes/Kismet/GameplayStatics.h"

// Sets default values
APlayerCharacter::APlayerCharacter() 
{
	UE_LOG(ARLog, Warning, TEXT("APlayerCharacter -1"));
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraArm"));
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));

	SpringArm->SetupAttachment(RootComponent); //
	SpringArm->SetRelativeLocation(FVector(0.f,80.f,80.f));
	//SpringArm->SetRelativeLocation(FVector(0.f, 0.f, 0.f));
	Camera->SetupAttachment(SpringArm);

	GetCapsuleComponent()->SetCollisionProfileName(TEXT("Player"));

	GetCapsuleComponent()->SetCapsuleHalfHeight(88.f);
	GetCapsuleComponent()->SetCapsuleRadius( 34.f);

	GetMesh()->SetRelativeLocationAndRotation(FVector(0.f, 0.f, -88.f), FRotator(0.f, -90.f, 0.f));

	//FMath::
	
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->JumpZVelocity = 600.f;
	
	//GetCharacterMovement()->CustomMovementMode
	fAttackRange=200.f;//공격사거리
	fAttackSize= FVector(70.f, 70.f, 70.f);


	static ConstructorHelpers::FObjectFinder<USkeletalMesh>	BodyAsset(TEXT("SkeletalMesh'/Game/ParagonWraith/Characters/Heroes/Wraith/Meshes/Wraith.Wraith'"));

	if (BodyAsset.Succeeded())
		GetMesh()->SetSkeletalMesh(BodyAsset.Object);


	static ConstructorHelpers::FClassFinder<UAnimInstance>	PlayerBluePrint(TEXT("AnimBlueprint'/Game/BluePrint/PlayerGunManAinmBP.PlayerGunManAinmBP_C'"));

	if (PlayerBluePrint.Succeeded())
		GetMesh()->SetAnimInstanceClass(PlayerBluePrint.Class);
	/*
	GetController()->GetControlRotation();
	GetVelocity();
	정규화중이었음 두벡터 사이의 각
	DELTAROTA*/


	//UI
	HPBarWidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("HPBARWIDGET"));
	HPBarWidgetComponent->SetupAttachment(GetMesh());
	
	HPBarWidgetComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 180.0f));
	HPBarWidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);//위젯을 어떤공간에 표현할것이냐. Screen에 표현한다. 
	static ConstructorHelpers::FClassFinder<UUserWidget> UI_HUD(TEXT("WidgetBlueprint'/Game/UI/test.test_C'"));

	if (UI_HUD.Succeeded())
	{
		HPBarWidgetComponent->SetWidgetClass(UI_HUD.Class);
		HPBarWidgetComponent->SetDrawSize(FVector2D(150.0f, 50.0f));
		HPBarWidgetComponent->bVisible = 0;
	}


	
	//파티클

	DashFIreL = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("DashParicleL"));
	DashFIreR = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("DashParicleR"));
	DashLight = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("DashParicleLight"));

	//FX_piston_r
	DashFIreL->AttachTo(GetMesh(), TEXT("FX_piston_l"));
	static ConstructorHelpers::FObjectFinder<UParticleSystem> DashPa(TEXT("ParticleSystem'/Game/ParagonWraith/FX/Particles/Abilities/Drone/FX/P_Wraith_Drone_Fire.P_Wraith_Drone_Fire'"));
	DashFIreL->SetTemplate(DashPa.Object);
	DashFIreL->SetRelativeScale3D(FVector(2.5f, 2.5f, 2.5f));
	DashFIreL->bAutoDestroy = false;
	
	
	DashFIreR->AttachTo(GetMesh(), TEXT("FX_piston_r"));
	DashFIreR->SetTemplate(DashPa.Object);
	DashFIreR->SetRelativeScale3D(FVector(2.5f, 2.5f, 2.5f));
	DashFIreR->bAutoDestroy = false;

	DashLight->AttachTo(GetMesh(), TEXT("FX_backpack"));
	static ConstructorHelpers::FObjectFinder<UParticleSystem> DashPaLight(TEXT("ParticleSystem'/Game/ParagonWraith/FX/Particles/Abilities/Ultimate/FX/P_ShadowPlane_Exit_Decals_Wraith.P_ShadowPlane_Exit_Decals_Wraith'"));
	DashLight->SetTemplate(DashPaLight.Object);
	DashLight->bAutoDestroy = false;

	//스팟라이트
	HandLight = CreateDefaultSubobject< USpotLightComponent>(TEXT("SpotHandLight"));
	HandLight->AttachTo(SpringArm, TEXT("HANDLIGHT"));
	HandLight->Intensity = 30000.f;
	HandLight->SetRelativeLocation(FVector(300.f, 0.f,0.f));

	// 헤드라이트 수정.
	HeadLight = CreateDefaultSubobject< USpotLightComponent>(TEXT("SpotHeadLight"));
	HeadLight->AttachTo(GetMesh(), TEXT("Head"));
	HeadLight->Intensity = 90000.f;
	HeadLight->SetRelativeLocationAndRotation(FVector(0.f, 50.f, 10.f), FRotator(-17.f, 80.f, 0.f));
	HeadLight->OuterConeAngle = 30.f;
	HeadLight->InnerConeAngle = 20.f;
	HeadLight->AttenuationRadius = 8000.f;
	//UPointLightComponent

	SetControlMode(0);
	m_pAnimInstance = nullptr;

	bInputAttack = false;
	bInputRangeAttack = false;
	iHP = 1000;
	iHPMax = 1000;
	iDead = false;
	bCanBeDamaged = true;
	fBaseDmage = 300;

	//UI
	fHPPercent = 1.00f;
	
	iItemSlotFirst=1;
	iItemSlotSecond=0;
	fDashCollTime = 0;

	fCharacterDirection = 0; //조중중 캐릭방향
	//31장 30분
	AIControllerClass = AShareAIController::StaticClass();
	//EAutoPossessAI는 이넘인데 들어가면 4가지가 있는데 
	//PlacedInWorldOrSpawned는 필드에 존재하거나 스폰을 했을경우 AI컨트롤러가 빙의할수있도록 처리하는 것이다.
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	
	obtype = OB_PLAYER;


	
}
void APlayerCharacter::PostInitializeComponents()
{
	UE_LOG(ARLog, Warning, TEXT("PostInitializeComponents -1"));
	Super::PostInitializeComponents();

	m_pAnimInstance = Cast<UPlayerAnimInstance>(GetMesh()->GetAnimInstance());
	
	m_pAnimInstance->OnHitCheck.AddUObject(this, &APlayerCharacter::OnAttack);

	m_pAnimInstance->OnRangeAttack.AddUObject(this, &APlayerCharacter::OnAttackRange);
	
	//GetUserWidgetObject 유저위젯 포인터타입을 리턴한다.
	//그리고 캐스트로 UPlayerHPWidget로 형변환해준다.
}


// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(ARLog, Warning, TEXT("Begin -1"));
	
	HPBarWidget = Cast<UPlayerHPBarWidgetV2>(HPBarWidgetComponent->GetUserWidgetObject());


}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//UE_LOG(ARLog, Warning, TEXT("Tick -1"));
	//fCharacterDirection = fCharacterDirection + DeltaTime * 10.f;
	//m_pAnimInstance->SetCharacterDirection(fCharacterDirection);

	if (fDashCollTime > 0)
	{
		fDashCollTime-= DeltaTime;
	}
	
	
	//캐릭터 로테이터 테스트
	float tpitch;
	float tyaw;
	if (GetControlRotation().Pitch > 180.f) tpitch = GetControlRotation().Pitch - 360.f;
	else tpitch = GetControlRotation().Pitch;
	if (GetControlRotation().Yaw > 180.f) tyaw = GetControlRotation().Yaw - 360.f;
	else tyaw = GetControlRotation().Yaw;
	m_pAnimInstance->SetPItchYaw(tpitch, tyaw);
	//UE_LOG(ARLog, Warning, TEXT("player :%f, :%f"), tpitch,


		//tyaw);



	//레이저 사이트 실패
	
	FVector aa= GetMesh()->GetSocketLocation(TEXT("gun_barrel"));
	//FVector bb
	/*DrawDebugLine(GetWorld(), FVector(GetActorLocation().X, GetActorLocation().Y,aa.Z), GetMesh()->GetSocketLocation(TEXT("gun_barrel")) + GetActorForwardVector()*1000.f, FColor::Red,
		false, 0.03f);
	  DrawDebugLine(GetWorld(), FVector(GetMesh()->GetSocketLocation(TEXT("gun_barrel")).X, GetMesh()->GetSocketLocation(TEXT("gun_barrel")).Y, aa.Z), GetMesh()->GetSocketLocation(TEXT("gun_barrel")) + GetActorForwardVector()*1000.f, FColor::Red,
		false, 0.03f);
	  DrawDebugLine(GetWorld(), FVector(GetMesh()->GetSocketLocation(TEXT("gun_barrel")).X, GetMesh()->GetSocketLocation(TEXT("gun_barrel")).Y, aa.Z), FVector(GetMesh()->GetSocketLocation(TEXT("gun_barrel")).X + GetActorForwardVector()*1000.f), FColor::Red,
		  false, 0.03f);*/
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &APlayerCharacter::UpDown);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &APlayerCharacter::LeftRight);
	PlayerInputComponent->BindAxis(TEXT("TurnRate"),this, &APlayerCharacter::Turn);
	PlayerInputComponent->BindAxis(TEXT("LookUpRate"), this, &APlayerCharacter::LookUp);
	PlayerInputComponent->BindAxis(TEXT("Attack_Range"), this, &APlayerCharacter::AttackRange);

	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &APlayerCharacter::Jumpup); //점프
	PlayerInputComponent->BindAction(TEXT("Attack_Near"), IE_Pressed, this, &APlayerCharacter::AttackNear); //근거리공격
	//PlayerInputComponent->BindAction(TEXT("Attack_Range"), IE_Pressed, this, &APlayerCharacter::AttackRange);//원거리
	PlayerInputComponent->BindAction(TEXT("Zum"), IE_Pressed, this, &APlayerCharacter::Zumupdown);
	PlayerInputComponent->BindAction(TEXT("Dash"), IE_Pressed, this, &APlayerCharacter::Dash);
	PlayerInputComponent->BindAction(TEXT("UseFirst"), IE_Pressed, this, &APlayerCharacter::UseFirstItem);
	PlayerInputComponent->BindAction(TEXT("UseSecond"), IE_Pressed, this, &APlayerCharacter::UseSecondItem);
	
}

//1번인자 : 피해 데미지
//2번인자 : 데미지 종류
//3번인자 : 공격하는 액터의 컨트롤러
//4번인자 : 데미지 전달을 위해 이용하는 도구
float APlayerCharacter::TakeDamage(float DamageAmount, FDamageEvent const & DamageEvent, 
	AController * EventInstigator, AActor * DamageCauser)
{
	

	
	float fComputeDamage = Super::TakeDamage(DamageAmount,DamageEvent,
		EventInstigator,DamageCauser);

	if (GetController() == EventInstigator) return DamageAmount;


	iHP -= (int32)fComputeDamage;

	UE_LOG(ARLog, Warning, TEXT("Damage:%f , Hp:%d , ComputeDamaged : %f "),DamageAmount,iHP,fComputeDamage);
	m_pAnimInstance->Hit(true);
	if (iHP <= 0)
	{
		iHP = 0;
		iDead = true;
		m_pAnimInstance->Hit(false);
		m_pAnimInstance->Dead();
		SetActorEnableCollision(false);

		if (IsPlayerControlled())
		{
			UGameplayStatics::OpenLevel(GetWorld(), TEXT("DieMenu"),false);
		}

	}
	HPBarWidget->SetValue((float)iHP, (float)iHPMax);
	fHPPercent = (float)iHP/ (float)iHPMax;

	if (IsPlayerControlled())
	{
		//UE_LOG(ARLog, Warning, TEXT("PlayerDamaged %.2f ,iHP %f ,MMiHP %f"), fHPPercent, iHP, iHPMax);
		//SetValue((float)iHP, (float)iHPMax);
		//여기에 HP적용
		//https://www.youtube.com/watch?v=Nt4W1B8cKy8  //1:12:00 참고중  1:47:51
		//ingame에 바인딩으로 직접 떄려박음.
		
	}

	//OnHPChanged.Broadcast();
	
	return fComputeDamage;
}

void APlayerCharacter::PossessedBy(AController * NewController)
{
	UE_LOG(ARLog, Warning, TEXT("PossessedBy -1"));
	Super::PossessedBy(NewController);



	if (!IsPlayerControlled())
	{
		
		GetCharacterMovement()->MaxWalkSpeed = 500.f;
	}
	else
	{
		GetCharacterMovement()->MaxWalkSpeed = 1000.f;
		//bCanBeDamaged = false;
	}
}

void APlayerCharacter::SetbInputAttack(float _bInputAttack)
{
	bInputAttack = _bInputAttack;
}

bool APlayerCharacter::GetbInputAttack() const
{
	return bInputAttack;
}

void APlayerCharacter::SetbInputRangeAttack(float _bInputAttack)
{
	bInputRangeAttack = _bInputAttack;
}

bool APlayerCharacter::GetbInputRangeAttack() const
{
	return bInputRangeAttack;
}

void APlayerCharacter::SetControlMode(int32 ControlMode)
{
	if (ControlMode == 0)
	{
		
		/*
	회전을 처리하는 것은 Pitch, Yaw, Roll로 처리한다.
	Pitch : 좌우를 기준으로 회전을 하게된다.(Y축)
	Yaw : 상하를 기준으로 회전을 한다.(Z축)
	Roll : 정면을 기준으로 회전한다.(X축)
	*/
		SpringArm->TargetArmLength = 200.0f;
		SpringArm->SetRelativeRotation(FRotator(0.f, 20.f, 0.f));
		SpringArm->bUsePawnControlRotation = true; //폰의 뷰 컨트롤 회전을 사용
		SpringArm->bInheritPitch = true;
		SpringArm->bInheritRoll = true;
		SpringArm->bInheritYaw = true;
		SpringArm->bDoCollisionTest = true;
		bUseControllerRotationYaw = false;
		
	}
}

void APlayerCharacter::LookUp(float NewAxisValue)
{
	AddControllerPitchInput(-NewAxisValue);
}

void APlayerCharacter::Turn(float NewAxisValue)
{
	AddControllerYawInput(NewAxisValue);
	
}

void APlayerCharacter::UpDown(float NewAxisValue)
{
	//if (NewAxisValue) m_pAnimInstance->PlayWalkMontage();

	if (NewAxisValue > 0.f&& bUseControllerRotationYaw)
	{
		//UE_LOG(ARLog, Warning, TEXT("ForWard"));
		
		if (fCharacterDirection >= 2)
		{
			fCharacterDirection -=2;
		}
		else if (fCharacterDirection <= -2)
		{
			fCharacterDirection += 2;
		}
		else
		{
			fCharacterDirection = 0;
		}
		
		m_pAnimInstance->SetCharacterDirection(fCharacterDirection);

	}
	else if (NewAxisValue < 0.f&& bUseControllerRotationYaw) {
		//UE_LOG(ARLog, Warning, TEXT("Back")); 

		if (fCharacterDirection>=0 && fCharacterDirection<178)
		{
			fCharacterDirection += 2;
		}
		else if (fCharacterDirection <= 0 && fCharacterDirection > -178)
		{
			fCharacterDirection -= 2;
		}
		else if (fCharacterDirection>=178)
		{
			fCharacterDirection = 180;
		}
		else if (fCharacterDirection <= -178)
		{
			fCharacterDirection = -180;
		}

		m_pAnimInstance->SetCharacterDirection(fCharacterDirection);



	}
	//UE_LOG(ARLog, Warning, TEXT("sssssssssssssssssssssssssssssss %f "), fCharacterDirection);
	FVector xy = FVector(FRotationMatrix(GetControlRotation()).GetUnitAxis(EAxis::X).X, FRotationMatrix(GetControlRotation()).GetUnitAxis(EAxis::X).Y, 0.f);
	xy.Normalize();
	AddMovementInput(xy, NewAxisValue);//단위 벡터를 구해서 넣어보자.
	
}
 
void APlayerCharacter::LeftRight(float NewAxisValue)
{
	//if (NewAxisValue) m_pAnimInstance->PlayWalkMontage();
	
	if (NewAxisValue > 0.f&& bUseControllerRotationYaw)
	{
		//UE_LOG(ARLog, Warning, TEXT("Right"));
		//bUseControllerRotationYaw = false;
		//m_pAnimInstance->SetCharacterDirection(90.f);

		if (fCharacterDirection == -180) fCharacterDirection = 180;

		if (fCharacterDirection >= 92)
		{
			fCharacterDirection -= 2;
		}
		else if (fCharacterDirection <= 88)
		{
			fCharacterDirection += 2;
		}
		else
		{
			fCharacterDirection = 90;
		}
		m_pAnimInstance->SetCharacterDirection(fCharacterDirection);
	}
	else if (NewAxisValue < 0.f && bUseControllerRotationYaw)
	{
		//UE_LOG(ARLog, Warning, TEXT("Left"));

		//m_pAnimInstance->SetCharacterDirection(-90.f);
		if (fCharacterDirection == 180) fCharacterDirection = -180;
		if (fCharacterDirection >= -88)
		{
			fCharacterDirection -= 2;
		}
		else if (fCharacterDirection <=-92  &&fCharacterDirection >= -180)
		{
			fCharacterDirection += 2;
		}
		else
		{
			fCharacterDirection = -90;
		}
		m_pAnimInstance->SetCharacterDirection(fCharacterDirection);
	}
	
	AddMovementInput(FRotationMatrix(GetControlRotation()).GetUnitAxis(EAxis::Y), NewAxisValue);
	//acos
	
}

void APlayerCharacter::AttackRange(float NewAxisValue)
{
	
	if (NewAxisValue)
	{
		if (!bUseControllerRotationYaw)
		{
			//공격상태

			m_pAnimInstance->SetWalkRate(0.8f);
			GetCharacterMovement()->MaxWalkSpeed = 650.f;
			bUseControllerRotationYaw = true;

			SpringArm->TargetArmLength = 150.0f;
			SpringArm->SetRelativeRotation(FRotator(400.f, 420.f, 400.f));

		}



		bInputRangeAttack = true;
	}
}

void APlayerCharacter::Zumupdown()
{
	if (!bUseControllerRotationYaw) 
	{
		//공격상태
		m_pAnimInstance->SetWalkRate(0.8f);
		GetCharacterMovement()->MaxWalkSpeed = 650.f;
		bUseControllerRotationYaw = true;

		SpringArm->TargetArmLength = 150.0f;
		SpringArm->SetRelativeRotation(FRotator(0.f, 620.f, 0.f));
		
	}
	else
	{
		//idle상태
		bUseControllerRotationYaw = false;
		m_pAnimInstance->SetWalkRate(1.0f);
		GetCharacterMovement()->MaxWalkSpeed = 1000.f;
		m_pAnimInstance->SetCharacterDirection(0.f);

		SpringArm->TargetArmLength = 200.0f;
		SpringArm->SetRelativeLocation(FVector(0.f, 80.f, 80.f));
	}

}

void APlayerCharacter::Dash()
{
	
	if (fDashCollTime<=0)
	{
		if (GetCharacterMovement()->IsFalling())
		{
			UE_LOG(ARLog, Warning, TEXT("jump dash"));
			GetCharacterMovement()->AddImpulse(FVector(GetActorForwardVector().X * 700, GetActorForwardVector().Y *700
				, GetActorForwardVector().Z * 700), true);
		}
		else
		{
			GetCharacterMovement()->AddImpulse(FVector(GetActorForwardVector().X * 1000, GetActorForwardVector().Y * 1000
				, GetActorForwardVector().Z * 1000 + 200.f), true);
		}

		fDashCollTime = 2.0f;

		
		DashFIreR->Activate();
		DashFIreL->Activate();
		DashLight->Activate();
	}
}

void APlayerCharacter::UseFirstItem()
{
	if (iItemSlotFirst == 1)
	{
		
		auto bullet = GetWorld()->SpawnActor<AItemBox>(AItemBox::StaticClass(),
			GetActorLocation() + GetActorForwardVector() * 200.f,
			GetControlRotation());

		iHP += 100.f;
		if (iHP > iHPMax) iHP = iHPMax;

		//UIHP 적용
		HPBarWidget->SetValue((float)iHP, (float)iHPMax);
		fHPPercent = (float)iHP / (float)iHPMax;


		SetItemSlotFirst(0);

	}
}

void APlayerCharacter::UseSecondItem()
{
	/*
	float SquareSum=GetActorLocation().X*GetActorLocation().X + GetActorLocation().Y*GetActorLocation().Y+
		GetActorLocation().Z*GetActorLocation().Z;
	float SsScale= FMath::InvSqrt(SquareSum);

	AddMovementInput(FVector(GetActorLocation().X*SsScale, GetActorLocation().Y*SsScale, GetActorLocation().Z*SsScale), 20.0f);
	*/
	if (bCanBeDamaged) bCanBeDamaged = false;
	else bCanBeDamaged = true;
}

void APlayerCharacter::Jumpup()
{
	UE_LOG(ARLog, Warning, TEXT("jump"));
	bJump = true;
	Jump();
	//

	//AddMovementInput(, 200.f);
	
}

void APlayerCharacter::OnAttack()
{
	UE_LOG(ARLog, Warning, TEXT("OnAttack "));

	
	// 1번인자 : FHitResult타입이다. 물리적 충돌이 탐지된 경우 관련된 정보를
	// 여기로 반환해준다.
	// 2번인자 : Start 탐색할 시작 위치
	// 3번인자 : End 탐색을 끝날 위치
	// 4번인자 : 탐색에 사용할 도형의 회전정보
	// 5번인자 : ECollisionChannel타입. 물리적인 충돌 감지에 사용할 트레이스 채널 정보
	// 6번인자 : FCollisionShape 타입. 도형 정보를 설정한다. 선, 구, 캡슐, 박스가 있다.
	// 7번인자 : FCollisionQueryParams 타입. 탐색할 방법에 대한 설정정보.
	// 8번인자 : FCollisionResponseParams 타입. 탐색 반응을 설정하기 위한 정보.
	//GetWorld()->SweepSingleByChannel(, , , , , , , );
	/*
	FHitResult HitResult;
	FCollisionQueryParams Params(NAME_None, false, this);

	bool bResult = GetWorld()->SweepSingleByChannel(HitResult, GetActorLocation(),
		GetActorLocation() + GetActorForwardVector()*200.f, FQuat::Identity, ECollisionChannel::ECC_GameTraceChannel2,
		FCollisionShape::MakeBox(FVector(50.f, 50.f, 50.f)), Params);

	if (bResult)
	{
		if (HitResult.Actor.IsValid())
		{
			UE_LOG(ARLog, Warning, TEXT("CollisionCube"));
		}
	}*/

	FCollisionQueryParams Params(NAME_None, false, this);

	TArray<FHitResult>	HitResultArr;

	bool bResult = GetWorld()->SweepMultiByChannel(HitResultArr, GetActorLocation(),
		GetActorLocation() + GetActorForwardVector()*fAttackRange, FQuat::Identity, ECollisionChannel::ECC_GameTraceChannel2,
		FCollisionShape::MakeBox(fAttackSize), Params);

	if (bResult)
	{
		for (auto HitResult : HitResultArr)
		{
			if (HitResult.Actor.IsValid())
			{
				UE_LOG(ARLog, Warning, TEXT("CollisionCube : %s"), *HitResult.Actor->GetName());
				FDamageEvent DamageEvent;
				HitResult.Actor->TakeDamage(fBaseDmage, DamageEvent, GetController(), this);
			}
		}
	}
//#이붙어있으니 전처리기
//디버그랑 릴리즈랑 체크해주는게 있음.
//ENABLE_DRAW_DEBUG 디버그 활성화가 되어있는지 체크해줌.
#if ENABLE_DRAW_DEBUG
	FVector vTrace = GetActorForwardVector()*fAttackRange;//방향과 거리를 나타냄
	FVector vCenter = GetActorLocation()+vTrace*0.5f;//가운데니까 절반의 거리
	float fHalfHeight = fAttackRange * 0.5f +0.f;
	FQuat CapsuleRot = FRotationMatrix::MakeFromZ(vTrace).ToQuat();
	FColor DrawColor = !bResult ? FColor::Green : FColor::Red;
	float fDebufLifeTime = 2.f;

	DrawDebugBox(GetWorld(), vCenter, fAttackSize, CapsuleRot,DrawColor,false, fDebufLifeTime);
	
	//DrawDebugCapule()
#endif
	
}

void APlayerCharacter::AttackNear()
{
	bInputAttack = true;
}

void APlayerCharacter::OnAttackRange()
{
	GetCharacterMovement()->AddImpulse(FVector(1000.f, 0.f, 0.f));
	UE_LOG(ARLog, Warning, TEXT("spawn "));
	auto bullet = GetWorld()->SpawnActor<ABullet>(ABullet::StaticClass(),
		GetMesh()->GetSocketLocation(TEXT("gun_barrel")) +GetActorForwardVector()*10.f,
		GetControlRotation());

	

	bullet->SetDir(Camera->GetForwardVector());
	bullet->SetSelfController(GetController());

}

void APlayerCharacter::AIAttack()
{
}

bool APlayerCharacter::GetJumpStart()	const
{
	return bJump;
	
}

int32 APlayerCharacter::GetObType() const
{
	return obtype;;
}

void APlayerCharacter::SetHP(float NewHP)
{
	iHP = NewHP;
}

void APlayerCharacter::SetItemSlotFirst(int32 newtem)
{
	iItemSlotFirst = newtem;
}

void APlayerCharacter::SetItemSlotSecond(int32 newtem)
{
	iItemSlotSecond = newtem;
	
}

float APlayerCharacter::getPercentHp() const
{
	return fHPPercent;
}

int32 APlayerCharacter::GetItemSlotFirst() const
{
	return iItemSlotFirst;
}

int32 APlayerCharacter::GetItemSlotSecond() const
{
	return iItemSlotSecond;
}

float APlayerCharacter::GetDashCollTime() const
{
	return fDashCollTime;
}

