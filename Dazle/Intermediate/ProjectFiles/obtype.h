#pragma once

enum OB_TYPE
{
	OB_NULL,
	OB_MONSTER,
	OB_PLAYER,
	OB_OBJECT
};