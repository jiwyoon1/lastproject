// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Dazle/BTTask_AttackMinion.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBTTask_AttackMinion() {}
// Cross Module References
	DAZLE_API UClass* Z_Construct_UClass_UBTTask_AttackMinion_NoRegister();
	DAZLE_API UClass* Z_Construct_UClass_UBTTask_AttackMinion();
	AIMODULE_API UClass* Z_Construct_UClass_UBTTaskNode();
	UPackage* Z_Construct_UPackage__Script_Dazle();
// End Cross Module References
	void UBTTask_AttackMinion::StaticRegisterNativesUBTTask_AttackMinion()
	{
	}
	UClass* Z_Construct_UClass_UBTTask_AttackMinion_NoRegister()
	{
		return UBTTask_AttackMinion::StaticClass();
	}
	struct Z_Construct_UClass_UBTTask_AttackMinion_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBTTask_AttackMinion_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBTTaskNode,
		(UObject* (*)())Z_Construct_UPackage__Script_Dazle,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBTTask_AttackMinion_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BTTask_AttackMinion.h" },
		{ "ModuleRelativePath", "BTTask_AttackMinion.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBTTask_AttackMinion_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBTTask_AttackMinion>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBTTask_AttackMinion_Statics::ClassParams = {
		&UBTTask_AttackMinion::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x001000A0u,
		nullptr, 0,
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UBTTask_AttackMinion_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UBTTask_AttackMinion_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBTTask_AttackMinion()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBTTask_AttackMinion_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBTTask_AttackMinion, 2348980303);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBTTask_AttackMinion(Z_Construct_UClass_UBTTask_AttackMinion, &UBTTask_AttackMinion::StaticClass, TEXT("/Script/Dazle"), TEXT("UBTTask_AttackMinion"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBTTask_AttackMinion);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
