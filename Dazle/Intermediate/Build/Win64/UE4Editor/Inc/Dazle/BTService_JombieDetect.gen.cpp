// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Dazle/BTService_JombieDetect.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBTService_JombieDetect() {}
// Cross Module References
	DAZLE_API UClass* Z_Construct_UClass_UBTService_JombieDetect_NoRegister();
	DAZLE_API UClass* Z_Construct_UClass_UBTService_JombieDetect();
	AIMODULE_API UClass* Z_Construct_UClass_UBTService();
	UPackage* Z_Construct_UPackage__Script_Dazle();
// End Cross Module References
	void UBTService_JombieDetect::StaticRegisterNativesUBTService_JombieDetect()
	{
	}
	UClass* Z_Construct_UClass_UBTService_JombieDetect_NoRegister()
	{
		return UBTService_JombieDetect::StaticClass();
	}
	struct Z_Construct_UClass_UBTService_JombieDetect_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBTService_JombieDetect_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBTService,
		(UObject* (*)())Z_Construct_UPackage__Script_Dazle,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBTService_JombieDetect_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BTService_JombieDetect.h" },
		{ "ModuleRelativePath", "BTService_JombieDetect.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBTService_JombieDetect_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBTService_JombieDetect>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBTService_JombieDetect_Statics::ClassParams = {
		&UBTService_JombieDetect::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x001000A0u,
		nullptr, 0,
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UBTService_JombieDetect_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UBTService_JombieDetect_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBTService_JombieDetect()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBTService_JombieDetect_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBTService_JombieDetect, 613308958);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBTService_JombieDetect(Z_Construct_UClass_UBTService_JombieDetect, &UBTService_JombieDetect::StaticClass, TEXT("/Script/Dazle"), TEXT("UBTService_JombieDetect"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBTService_JombieDetect);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
