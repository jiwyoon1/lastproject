// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_PawnFirstMinion_generated_h
#error "PawnFirstMinion.generated.h already included, missing '#pragma once' in PawnFirstMinion.h"
#endif
#define DAZLE_PawnFirstMinion_generated_h

#define Dazle_Source_Dazle_PawnFirstMinion_h_12_RPC_WRAPPERS
#define Dazle_Source_Dazle_PawnFirstMinion_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Dazle_Source_Dazle_PawnFirstMinion_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPawnFirstMinion(); \
	friend struct Z_Construct_UClass_APawnFirstMinion_Statics; \
public: \
	DECLARE_CLASS(APawnFirstMinion, APawn, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(APawnFirstMinion)


#define Dazle_Source_Dazle_PawnFirstMinion_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPawnFirstMinion(); \
	friend struct Z_Construct_UClass_APawnFirstMinion_Statics; \
public: \
	DECLARE_CLASS(APawnFirstMinion, APawn, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(APawnFirstMinion)


#define Dazle_Source_Dazle_PawnFirstMinion_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APawnFirstMinion(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APawnFirstMinion) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APawnFirstMinion); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APawnFirstMinion); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APawnFirstMinion(APawnFirstMinion&&); \
	NO_API APawnFirstMinion(const APawnFirstMinion&); \
public:


#define Dazle_Source_Dazle_PawnFirstMinion_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APawnFirstMinion(APawnFirstMinion&&); \
	NO_API APawnFirstMinion(const APawnFirstMinion&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APawnFirstMinion); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APawnFirstMinion); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APawnFirstMinion)


#define Dazle_Source_Dazle_PawnFirstMinion_h_12_PRIVATE_PROPERTY_OFFSET
#define Dazle_Source_Dazle_PawnFirstMinion_h_9_PROLOG
#define Dazle_Source_Dazle_PawnFirstMinion_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_PawnFirstMinion_h_12_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_PawnFirstMinion_h_12_RPC_WRAPPERS \
	Dazle_Source_Dazle_PawnFirstMinion_h_12_INCLASS \
	Dazle_Source_Dazle_PawnFirstMinion_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_PawnFirstMinion_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_PawnFirstMinion_h_12_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_PawnFirstMinion_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_PawnFirstMinion_h_12_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_PawnFirstMinion_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_PawnFirstMinion_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
