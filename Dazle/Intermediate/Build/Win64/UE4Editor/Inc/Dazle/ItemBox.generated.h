// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef DAZLE_ItemBox_generated_h
#error "ItemBox.generated.h already included, missing '#pragma once' in ItemBox.h"
#endif
#define DAZLE_ItemBox_generated_h

#define Dazle_Source_Dazle_ItemBox_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnCharacterOverlap) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnCharacterOverlap(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define Dazle_Source_Dazle_ItemBox_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnCharacterOverlap) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnCharacterOverlap(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define Dazle_Source_Dazle_ItemBox_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAItemBox(); \
	friend struct Z_Construct_UClass_AItemBox_Statics; \
public: \
	DECLARE_CLASS(AItemBox, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(AItemBox)


#define Dazle_Source_Dazle_ItemBox_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAItemBox(); \
	friend struct Z_Construct_UClass_AItemBox_Statics; \
public: \
	DECLARE_CLASS(AItemBox, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(AItemBox)


#define Dazle_Source_Dazle_ItemBox_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AItemBox(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AItemBox) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AItemBox); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AItemBox); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AItemBox(AItemBox&&); \
	NO_API AItemBox(const AItemBox&); \
public:


#define Dazle_Source_Dazle_ItemBox_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AItemBox(AItemBox&&); \
	NO_API AItemBox(const AItemBox&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AItemBox); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AItemBox); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AItemBox)


#define Dazle_Source_Dazle_ItemBox_h_12_PRIVATE_PROPERTY_OFFSET
#define Dazle_Source_Dazle_ItemBox_h_9_PROLOG
#define Dazle_Source_Dazle_ItemBox_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_ItemBox_h_12_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_ItemBox_h_12_RPC_WRAPPERS \
	Dazle_Source_Dazle_ItemBox_h_12_INCLASS \
	Dazle_Source_Dazle_ItemBox_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_ItemBox_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_ItemBox_h_12_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_ItemBox_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_ItemBox_h_12_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_ItemBox_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_ItemBox_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
