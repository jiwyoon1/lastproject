// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_JombieController_generated_h
#error "JombieController.generated.h already included, missing '#pragma once' in JombieController.h"
#endif
#define DAZLE_JombieController_generated_h

#define Dazle_Source_Dazle_JombieController_h_15_RPC_WRAPPERS
#define Dazle_Source_Dazle_JombieController_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Dazle_Source_Dazle_JombieController_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAJombieController(); \
	friend struct Z_Construct_UClass_AJombieController_Statics; \
public: \
	DECLARE_CLASS(AJombieController, AAIController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(AJombieController)


#define Dazle_Source_Dazle_JombieController_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAJombieController(); \
	friend struct Z_Construct_UClass_AJombieController_Statics; \
public: \
	DECLARE_CLASS(AJombieController, AAIController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(AJombieController)


#define Dazle_Source_Dazle_JombieController_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AJombieController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AJombieController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AJombieController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AJombieController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AJombieController(AJombieController&&); \
	NO_API AJombieController(const AJombieController&); \
public:


#define Dazle_Source_Dazle_JombieController_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AJombieController(AJombieController&&); \
	NO_API AJombieController(const AJombieController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AJombieController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AJombieController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AJombieController)


#define Dazle_Source_Dazle_JombieController_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__pBTree() { return STRUCT_OFFSET(AJombieController, pBTree); } \
	FORCEINLINE static uint32 __PPO__pBBData() { return STRUCT_OFFSET(AJombieController, pBBData); }


#define Dazle_Source_Dazle_JombieController_h_12_PROLOG
#define Dazle_Source_Dazle_JombieController_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_JombieController_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_JombieController_h_15_RPC_WRAPPERS \
	Dazle_Source_Dazle_JombieController_h_15_INCLASS \
	Dazle_Source_Dazle_JombieController_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_JombieController_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_JombieController_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_JombieController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_JombieController_h_15_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_JombieController_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_JombieController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
