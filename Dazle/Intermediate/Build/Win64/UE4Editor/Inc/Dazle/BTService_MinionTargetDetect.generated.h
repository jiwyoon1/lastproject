// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_BTService_MinionTargetDetect_generated_h
#error "BTService_MinionTargetDetect.generated.h already included, missing '#pragma once' in BTService_MinionTargetDetect.h"
#endif
#define DAZLE_BTService_MinionTargetDetect_generated_h

#define Dazle_Source_Dazle_BTService_MinionTargetDetect_h_15_RPC_WRAPPERS
#define Dazle_Source_Dazle_BTService_MinionTargetDetect_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Dazle_Source_Dazle_BTService_MinionTargetDetect_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBTService_MinionTargetDetect(); \
	friend struct Z_Construct_UClass_UBTService_MinionTargetDetect_Statics; \
public: \
	DECLARE_CLASS(UBTService_MinionTargetDetect, UBTService, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UBTService_MinionTargetDetect)


#define Dazle_Source_Dazle_BTService_MinionTargetDetect_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUBTService_MinionTargetDetect(); \
	friend struct Z_Construct_UClass_UBTService_MinionTargetDetect_Statics; \
public: \
	DECLARE_CLASS(UBTService_MinionTargetDetect, UBTService, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UBTService_MinionTargetDetect)


#define Dazle_Source_Dazle_BTService_MinionTargetDetect_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTService_MinionTargetDetect(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTService_MinionTargetDetect) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTService_MinionTargetDetect); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTService_MinionTargetDetect); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTService_MinionTargetDetect(UBTService_MinionTargetDetect&&); \
	NO_API UBTService_MinionTargetDetect(const UBTService_MinionTargetDetect&); \
public:


#define Dazle_Source_Dazle_BTService_MinionTargetDetect_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTService_MinionTargetDetect(UBTService_MinionTargetDetect&&); \
	NO_API UBTService_MinionTargetDetect(const UBTService_MinionTargetDetect&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTService_MinionTargetDetect); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTService_MinionTargetDetect); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UBTService_MinionTargetDetect)


#define Dazle_Source_Dazle_BTService_MinionTargetDetect_h_15_PRIVATE_PROPERTY_OFFSET
#define Dazle_Source_Dazle_BTService_MinionTargetDetect_h_12_PROLOG
#define Dazle_Source_Dazle_BTService_MinionTargetDetect_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_BTService_MinionTargetDetect_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_BTService_MinionTargetDetect_h_15_RPC_WRAPPERS \
	Dazle_Source_Dazle_BTService_MinionTargetDetect_h_15_INCLASS \
	Dazle_Source_Dazle_BTService_MinionTargetDetect_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_BTService_MinionTargetDetect_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_BTService_MinionTargetDetect_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_BTService_MinionTargetDetect_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_BTService_MinionTargetDetect_h_15_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_BTService_MinionTargetDetect_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_BTService_MinionTargetDetect_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
