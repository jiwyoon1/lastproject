// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_BTTask_MoveTarget_generated_h
#error "BTTask_MoveTarget.generated.h already included, missing '#pragma once' in BTTask_MoveTarget.h"
#endif
#define DAZLE_BTTask_MoveTarget_generated_h

#define Dazle_Source_Dazle_BTTask_MoveTarget_h_15_RPC_WRAPPERS
#define Dazle_Source_Dazle_BTTask_MoveTarget_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Dazle_Source_Dazle_BTTask_MoveTarget_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBTTask_MoveTarget(); \
	friend struct Z_Construct_UClass_UBTTask_MoveTarget_Statics; \
public: \
	DECLARE_CLASS(UBTTask_MoveTarget, UBTTaskNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UBTTask_MoveTarget)


#define Dazle_Source_Dazle_BTTask_MoveTarget_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUBTTask_MoveTarget(); \
	friend struct Z_Construct_UClass_UBTTask_MoveTarget_Statics; \
public: \
	DECLARE_CLASS(UBTTask_MoveTarget, UBTTaskNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UBTTask_MoveTarget)


#define Dazle_Source_Dazle_BTTask_MoveTarget_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTTask_MoveTarget(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTTask_MoveTarget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTask_MoveTarget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTask_MoveTarget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTask_MoveTarget(UBTTask_MoveTarget&&); \
	NO_API UBTTask_MoveTarget(const UBTTask_MoveTarget&); \
public:


#define Dazle_Source_Dazle_BTTask_MoveTarget_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTask_MoveTarget(UBTTask_MoveTarget&&); \
	NO_API UBTTask_MoveTarget(const UBTTask_MoveTarget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTask_MoveTarget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTask_MoveTarget); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UBTTask_MoveTarget)


#define Dazle_Source_Dazle_BTTask_MoveTarget_h_15_PRIVATE_PROPERTY_OFFSET
#define Dazle_Source_Dazle_BTTask_MoveTarget_h_12_PROLOG
#define Dazle_Source_Dazle_BTTask_MoveTarget_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_BTTask_MoveTarget_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_BTTask_MoveTarget_h_15_RPC_WRAPPERS \
	Dazle_Source_Dazle_BTTask_MoveTarget_h_15_INCLASS \
	Dazle_Source_Dazle_BTTask_MoveTarget_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_BTTask_MoveTarget_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_BTTask_MoveTarget_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_BTTask_MoveTarget_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_BTTask_MoveTarget_h_15_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_BTTask_MoveTarget_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_BTTask_MoveTarget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
