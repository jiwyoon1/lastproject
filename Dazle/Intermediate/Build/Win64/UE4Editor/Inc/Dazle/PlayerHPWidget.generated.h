// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_PlayerHPWidget_generated_h
#error "PlayerHPWidget.generated.h already included, missing '#pragma once' in PlayerHPWidget.h"
#endif
#define DAZLE_PlayerHPWidget_generated_h

#define Dazle_Source_Dazle_PlayerHPWidget_h_17_RPC_WRAPPERS
#define Dazle_Source_Dazle_PlayerHPWidget_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Dazle_Source_Dazle_PlayerHPWidget_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPlayerHPWidget(); \
	friend struct Z_Construct_UClass_UPlayerHPWidget_Statics; \
public: \
	DECLARE_CLASS(UPlayerHPWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UPlayerHPWidget)


#define Dazle_Source_Dazle_PlayerHPWidget_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUPlayerHPWidget(); \
	friend struct Z_Construct_UClass_UPlayerHPWidget_Statics; \
public: \
	DECLARE_CLASS(UPlayerHPWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UPlayerHPWidget)


#define Dazle_Source_Dazle_PlayerHPWidget_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlayerHPWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlayerHPWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlayerHPWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlayerHPWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlayerHPWidget(UPlayerHPWidget&&); \
	NO_API UPlayerHPWidget(const UPlayerHPWidget&); \
public:


#define Dazle_Source_Dazle_PlayerHPWidget_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlayerHPWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlayerHPWidget(UPlayerHPWidget&&); \
	NO_API UPlayerHPWidget(const UPlayerHPWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlayerHPWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlayerHPWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlayerHPWidget)


#define Dazle_Source_Dazle_PlayerHPWidget_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__m_fValue() { return STRUCT_OFFSET(UPlayerHPWidget, m_fValue); } \
	FORCEINLINE static uint32 __PPO__ProgressBar() { return STRUCT_OFFSET(UPlayerHPWidget, ProgressBar); }


#define Dazle_Source_Dazle_PlayerHPWidget_h_14_PROLOG
#define Dazle_Source_Dazle_PlayerHPWidget_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_PlayerHPWidget_h_17_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_PlayerHPWidget_h_17_RPC_WRAPPERS \
	Dazle_Source_Dazle_PlayerHPWidget_h_17_INCLASS \
	Dazle_Source_Dazle_PlayerHPWidget_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_PlayerHPWidget_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_PlayerHPWidget_h_17_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_PlayerHPWidget_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_PlayerHPWidget_h_17_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_PlayerHPWidget_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_PlayerHPWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
