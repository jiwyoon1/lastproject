// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Dazle/GranadeBoom.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGranadeBoom() {}
// Cross Module References
	DAZLE_API UClass* Z_Construct_UClass_AGranadeBoom_NoRegister();
	DAZLE_API UClass* Z_Construct_UClass_AGranadeBoom();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Dazle();
	ENGINE_API UClass* Z_Construct_UClass_URadialForceComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UParticleSystemComponent_NoRegister();
// End Cross Module References
	void AGranadeBoom::StaticRegisterNativesAGranadeBoom()
	{
	}
	UClass* Z_Construct_UClass_AGranadeBoom_NoRegister()
	{
		return AGranadeBoom::StaticClass();
	}
	struct Z_Construct_UClass_AGranadeBoom_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RadiForce_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RadiForce;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExplosionParticle_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ExplosionParticle;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AGranadeBoom_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Dazle,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGranadeBoom_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GranadeBoom.h" },
		{ "ModuleRelativePath", "GranadeBoom.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGranadeBoom_Statics::NewProp_RadiForce_MetaData[] = {
		{ "Category", "Boom" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "GranadeBoom.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AGranadeBoom_Statics::NewProp_RadiForce = { UE4CodeGen_Private::EPropertyClass::Object, "RadiForce", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000080009, 1, nullptr, STRUCT_OFFSET(AGranadeBoom, RadiForce), Z_Construct_UClass_URadialForceComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AGranadeBoom_Statics::NewProp_RadiForce_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGranadeBoom_Statics::NewProp_RadiForce_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGranadeBoom_Statics::NewProp_ExplosionParticle_MetaData[] = {
		{ "Category", "Particle" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "GranadeBoom.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AGranadeBoom_Statics::NewProp_ExplosionParticle = { UE4CodeGen_Private::EPropertyClass::Object, "ExplosionParticle", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000080009, 1, nullptr, STRUCT_OFFSET(AGranadeBoom, ExplosionParticle), Z_Construct_UClass_UParticleSystemComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AGranadeBoom_Statics::NewProp_ExplosionParticle_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGranadeBoom_Statics::NewProp_ExplosionParticle_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AGranadeBoom_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGranadeBoom_Statics::NewProp_RadiForce,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGranadeBoom_Statics::NewProp_ExplosionParticle,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AGranadeBoom_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AGranadeBoom>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AGranadeBoom_Statics::ClassParams = {
		&AGranadeBoom::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		nullptr, 0,
		Z_Construct_UClass_AGranadeBoom_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_AGranadeBoom_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AGranadeBoom_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AGranadeBoom_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AGranadeBoom()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AGranadeBoom_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AGranadeBoom, 3779368916);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGranadeBoom(Z_Construct_UClass_AGranadeBoom, &AGranadeBoom::StaticClass, TEXT("/Script/Dazle"), TEXT("AGranadeBoom"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGranadeBoom);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
