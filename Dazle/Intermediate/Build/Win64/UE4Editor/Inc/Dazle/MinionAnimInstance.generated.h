// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_MinionAnimInstance_generated_h
#error "MinionAnimInstance.generated.h already included, missing '#pragma once' in MinionAnimInstance.h"
#endif
#define DAZLE_MinionAnimInstance_generated_h

#define Dazle_Source_Dazle_MinionAnimInstance_h_31_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAnimNotify_Dead_End) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Dead_End(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Hit_End) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Hit_End(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Attack_End) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Attack_End(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Attack_Start) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Attack_Start(); \
		P_NATIVE_END; \
	}


#define Dazle_Source_Dazle_MinionAnimInstance_h_31_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAnimNotify_Dead_End) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Dead_End(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Hit_End) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Hit_End(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Attack_End) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Attack_End(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Attack_Start) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Attack_Start(); \
		P_NATIVE_END; \
	}


#define Dazle_Source_Dazle_MinionAnimInstance_h_31_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMinionAnimInstance(); \
	friend struct Z_Construct_UClass_UMinionAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UMinionAnimInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UMinionAnimInstance)


#define Dazle_Source_Dazle_MinionAnimInstance_h_31_INCLASS \
private: \
	static void StaticRegisterNativesUMinionAnimInstance(); \
	friend struct Z_Construct_UClass_UMinionAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UMinionAnimInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UMinionAnimInstance)


#define Dazle_Source_Dazle_MinionAnimInstance_h_31_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMinionAnimInstance(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMinionAnimInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMinionAnimInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMinionAnimInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMinionAnimInstance(UMinionAnimInstance&&); \
	NO_API UMinionAnimInstance(const UMinionAnimInstance&); \
public:


#define Dazle_Source_Dazle_MinionAnimInstance_h_31_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMinionAnimInstance(UMinionAnimInstance&&); \
	NO_API UMinionAnimInstance(const UMinionAnimInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMinionAnimInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMinionAnimInstance); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMinionAnimInstance)


#define Dazle_Source_Dazle_MinionAnimInstance_h_31_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bAttack() { return STRUCT_OFFSET(UMinionAnimInstance, bAttack); } \
	FORCEINLINE static uint32 __PPO__bIdle() { return STRUCT_OFFSET(UMinionAnimInstance, bIdle); } \
	FORCEINLINE static uint32 __PPO__characterSpeed() { return STRUCT_OFFSET(UMinionAnimInstance, characterSpeed); } \
	FORCEINLINE static uint32 __PPO__characterDirection() { return STRUCT_OFFSET(UMinionAnimInstance, characterDirection); } \
	FORCEINLINE static uint32 __PPO__iBaseAnimState() { return STRUCT_OFFSET(UMinionAnimInstance, iBaseAnimState); } \
	FORCEINLINE static uint32 __PPO__fVelocity() { return STRUCT_OFFSET(UMinionAnimInstance, fVelocity); } \
	FORCEINLINE static uint32 __PPO__bJump() { return STRUCT_OFFSET(UMinionAnimInstance, bJump); } \
	FORCEINLINE static uint32 __PPO__attackswich() { return STRUCT_OFFSET(UMinionAnimInstance, attackswich); } \
	FORCEINLINE static uint32 __PPO__bDamage() { return STRUCT_OFFSET(UMinionAnimInstance, bDamage); } \
	FORCEINLINE static uint32 __PPO__bDead() { return STRUCT_OFFSET(UMinionAnimInstance, bDead); }


#define Dazle_Source_Dazle_MinionAnimInstance_h_28_PROLOG
#define Dazle_Source_Dazle_MinionAnimInstance_h_31_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_MinionAnimInstance_h_31_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_MinionAnimInstance_h_31_RPC_WRAPPERS \
	Dazle_Source_Dazle_MinionAnimInstance_h_31_INCLASS \
	Dazle_Source_Dazle_MinionAnimInstance_h_31_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_MinionAnimInstance_h_31_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_MinionAnimInstance_h_31_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_MinionAnimInstance_h_31_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_MinionAnimInstance_h_31_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_MinionAnimInstance_h_31_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_MinionAnimInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
