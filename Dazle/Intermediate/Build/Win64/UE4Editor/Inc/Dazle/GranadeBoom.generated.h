// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_GranadeBoom_generated_h
#error "GranadeBoom.generated.h already included, missing '#pragma once' in GranadeBoom.h"
#endif
#define DAZLE_GranadeBoom_generated_h

#define Dazle_Source_Dazle_GranadeBoom_h_14_RPC_WRAPPERS
#define Dazle_Source_Dazle_GranadeBoom_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Dazle_Source_Dazle_GranadeBoom_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGranadeBoom(); \
	friend struct Z_Construct_UClass_AGranadeBoom_Statics; \
public: \
	DECLARE_CLASS(AGranadeBoom, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(AGranadeBoom)


#define Dazle_Source_Dazle_GranadeBoom_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAGranadeBoom(); \
	friend struct Z_Construct_UClass_AGranadeBoom_Statics; \
public: \
	DECLARE_CLASS(AGranadeBoom, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(AGranadeBoom)


#define Dazle_Source_Dazle_GranadeBoom_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGranadeBoom(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGranadeBoom) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGranadeBoom); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGranadeBoom); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGranadeBoom(AGranadeBoom&&); \
	NO_API AGranadeBoom(const AGranadeBoom&); \
public:


#define Dazle_Source_Dazle_GranadeBoom_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGranadeBoom(AGranadeBoom&&); \
	NO_API AGranadeBoom(const AGranadeBoom&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGranadeBoom); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGranadeBoom); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGranadeBoom)


#define Dazle_Source_Dazle_GranadeBoom_h_14_PRIVATE_PROPERTY_OFFSET
#define Dazle_Source_Dazle_GranadeBoom_h_11_PROLOG
#define Dazle_Source_Dazle_GranadeBoom_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_GranadeBoom_h_14_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_GranadeBoom_h_14_RPC_WRAPPERS \
	Dazle_Source_Dazle_GranadeBoom_h_14_INCLASS \
	Dazle_Source_Dazle_GranadeBoom_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_GranadeBoom_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_GranadeBoom_h_14_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_GranadeBoom_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_GranadeBoom_h_14_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_GranadeBoom_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_GranadeBoom_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
