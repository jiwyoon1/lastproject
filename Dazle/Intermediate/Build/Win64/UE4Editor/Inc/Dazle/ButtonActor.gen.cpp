// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Dazle/ButtonActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeButtonActor() {}
// Cross Module References
	DAZLE_API UClass* Z_Construct_UClass_AButtonActor_NoRegister();
	DAZLE_API UClass* Z_Construct_UClass_AButtonActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Dazle();
	DAZLE_API UClass* Z_Construct_UClass_UButtonWidget_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UWidgetComponent_NoRegister();
// End Cross Module References
	void AButtonActor::StaticRegisterNativesAButtonActor()
	{
	}
	UClass* Z_Construct_UClass_AButtonActor_NoRegister()
	{
		return AButtonActor::StaticClass();
	}
	struct Z_Construct_UClass_AButtonActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ButtonWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ButtonWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ButtonWidgetComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ButtonWidgetComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AButtonActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Dazle,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AButtonActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ButtonActor.h" },
		{ "ModuleRelativePath", "ButtonActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AButtonActor_Statics::NewProp_ButtonWidget_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "ButtonActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AButtonActor_Statics::NewProp_ButtonWidget = { UE4CodeGen_Private::EPropertyClass::Object, "ButtonWidget", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000080008, 1, nullptr, STRUCT_OFFSET(AButtonActor, ButtonWidget), Z_Construct_UClass_UButtonWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AButtonActor_Statics::NewProp_ButtonWidget_MetaData, ARRAY_COUNT(Z_Construct_UClass_AButtonActor_Statics::NewProp_ButtonWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AButtonActor_Statics::NewProp_ButtonWidgetComponent_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "ButtonActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AButtonActor_Statics::NewProp_ButtonWidgetComponent = { UE4CodeGen_Private::EPropertyClass::Object, "ButtonWidgetComponent", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000080008, 1, nullptr, STRUCT_OFFSET(AButtonActor, ButtonWidgetComponent), Z_Construct_UClass_UWidgetComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AButtonActor_Statics::NewProp_ButtonWidgetComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_AButtonActor_Statics::NewProp_ButtonWidgetComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AButtonActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AButtonActor_Statics::NewProp_ButtonWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AButtonActor_Statics::NewProp_ButtonWidgetComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AButtonActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AButtonActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AButtonActor_Statics::ClassParams = {
		&AButtonActor::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		nullptr, 0,
		Z_Construct_UClass_AButtonActor_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_AButtonActor_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AButtonActor_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AButtonActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AButtonActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AButtonActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AButtonActor, 2508570429);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AButtonActor(Z_Construct_UClass_AButtonActor, &AButtonActor::StaticClass, TEXT("/Script/Dazle"), TEXT("AButtonActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AButtonActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
