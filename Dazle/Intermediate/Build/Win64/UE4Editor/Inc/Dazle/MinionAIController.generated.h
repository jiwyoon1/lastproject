// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_MinionAIController_generated_h
#error "MinionAIController.generated.h already included, missing '#pragma once' in MinionAIController.h"
#endif
#define DAZLE_MinionAIController_generated_h

#define Dazle_Source_Dazle_MinionAIController_h_15_RPC_WRAPPERS
#define Dazle_Source_Dazle_MinionAIController_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Dazle_Source_Dazle_MinionAIController_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMinionAIController(); \
	friend struct Z_Construct_UClass_AMinionAIController_Statics; \
public: \
	DECLARE_CLASS(AMinionAIController, AAIController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(AMinionAIController)


#define Dazle_Source_Dazle_MinionAIController_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAMinionAIController(); \
	friend struct Z_Construct_UClass_AMinionAIController_Statics; \
public: \
	DECLARE_CLASS(AMinionAIController, AAIController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(AMinionAIController)


#define Dazle_Source_Dazle_MinionAIController_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMinionAIController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMinionAIController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMinionAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMinionAIController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMinionAIController(AMinionAIController&&); \
	NO_API AMinionAIController(const AMinionAIController&); \
public:


#define Dazle_Source_Dazle_MinionAIController_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMinionAIController(AMinionAIController&&); \
	NO_API AMinionAIController(const AMinionAIController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMinionAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMinionAIController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMinionAIController)


#define Dazle_Source_Dazle_MinionAIController_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__pBTree() { return STRUCT_OFFSET(AMinionAIController, pBTree); } \
	FORCEINLINE static uint32 __PPO__pBBData() { return STRUCT_OFFSET(AMinionAIController, pBBData); }


#define Dazle_Source_Dazle_MinionAIController_h_12_PROLOG
#define Dazle_Source_Dazle_MinionAIController_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_MinionAIController_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_MinionAIController_h_15_RPC_WRAPPERS \
	Dazle_Source_Dazle_MinionAIController_h_15_INCLASS \
	Dazle_Source_Dazle_MinionAIController_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_MinionAIController_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_MinionAIController_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_MinionAIController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_MinionAIController_h_15_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_MinionAIController_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_MinionAIController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
