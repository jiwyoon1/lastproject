// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_TestGameMode_generated_h
#error "TestGameMode.generated.h already included, missing '#pragma once' in TestGameMode.h"
#endif
#define DAZLE_TestGameMode_generated_h

#define Dazle_Source_Dazle_TestGameMode_h_15_RPC_WRAPPERS
#define Dazle_Source_Dazle_TestGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Dazle_Source_Dazle_TestGameMode_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATestGameMode(); \
	friend struct Z_Construct_UClass_ATestGameMode_Statics; \
public: \
	DECLARE_CLASS(ATestGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(ATestGameMode)


#define Dazle_Source_Dazle_TestGameMode_h_15_INCLASS \
private: \
	static void StaticRegisterNativesATestGameMode(); \
	friend struct Z_Construct_UClass_ATestGameMode_Statics; \
public: \
	DECLARE_CLASS(ATestGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(ATestGameMode)


#define Dazle_Source_Dazle_TestGameMode_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATestGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATestGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATestGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATestGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATestGameMode(ATestGameMode&&); \
	NO_API ATestGameMode(const ATestGameMode&); \
public:


#define Dazle_Source_Dazle_TestGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATestGameMode(ATestGameMode&&); \
	NO_API ATestGameMode(const ATestGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATestGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATestGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATestGameMode)


#define Dazle_Source_Dazle_TestGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__inGameWidget() { return STRUCT_OFFSET(ATestGameMode, inGameWidget); } \
	FORCEINLINE static uint32 __PPO__inGameWidgetClass() { return STRUCT_OFFSET(ATestGameMode, inGameWidgetClass); }


#define Dazle_Source_Dazle_TestGameMode_h_12_PROLOG
#define Dazle_Source_Dazle_TestGameMode_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_TestGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_TestGameMode_h_15_RPC_WRAPPERS \
	Dazle_Source_Dazle_TestGameMode_h_15_INCLASS \
	Dazle_Source_Dazle_TestGameMode_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_TestGameMode_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_TestGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_TestGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_TestGameMode_h_15_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_TestGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_TestGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
