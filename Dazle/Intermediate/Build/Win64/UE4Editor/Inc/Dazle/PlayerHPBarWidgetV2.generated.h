// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_PlayerHPBarWidgetV2_generated_h
#error "PlayerHPBarWidgetV2.generated.h already included, missing '#pragma once' in PlayerHPBarWidgetV2.h"
#endif
#define DAZLE_PlayerHPBarWidgetV2_generated_h

#define Dazle_Source_Dazle_PlayerHPBarWidgetV2_h_15_RPC_WRAPPERS
#define Dazle_Source_Dazle_PlayerHPBarWidgetV2_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Dazle_Source_Dazle_PlayerHPBarWidgetV2_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPlayerHPBarWidgetV2(); \
	friend struct Z_Construct_UClass_UPlayerHPBarWidgetV2_Statics; \
public: \
	DECLARE_CLASS(UPlayerHPBarWidgetV2, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UPlayerHPBarWidgetV2)


#define Dazle_Source_Dazle_PlayerHPBarWidgetV2_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUPlayerHPBarWidgetV2(); \
	friend struct Z_Construct_UClass_UPlayerHPBarWidgetV2_Statics; \
public: \
	DECLARE_CLASS(UPlayerHPBarWidgetV2, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UPlayerHPBarWidgetV2)


#define Dazle_Source_Dazle_PlayerHPBarWidgetV2_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlayerHPBarWidgetV2(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlayerHPBarWidgetV2) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlayerHPBarWidgetV2); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlayerHPBarWidgetV2); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlayerHPBarWidgetV2(UPlayerHPBarWidgetV2&&); \
	NO_API UPlayerHPBarWidgetV2(const UPlayerHPBarWidgetV2&); \
public:


#define Dazle_Source_Dazle_PlayerHPBarWidgetV2_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlayerHPBarWidgetV2(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlayerHPBarWidgetV2(UPlayerHPBarWidgetV2&&); \
	NO_API UPlayerHPBarWidgetV2(const UPlayerHPBarWidgetV2&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlayerHPBarWidgetV2); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlayerHPBarWidgetV2); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlayerHPBarWidgetV2)


#define Dazle_Source_Dazle_PlayerHPBarWidgetV2_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__m_fValue() { return STRUCT_OFFSET(UPlayerHPBarWidgetV2, m_fValue); } \
	FORCEINLINE static uint32 __PPO__ProgressBar() { return STRUCT_OFFSET(UPlayerHPBarWidgetV2, ProgressBar); }


#define Dazle_Source_Dazle_PlayerHPBarWidgetV2_h_12_PROLOG
#define Dazle_Source_Dazle_PlayerHPBarWidgetV2_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_PlayerHPBarWidgetV2_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_PlayerHPBarWidgetV2_h_15_RPC_WRAPPERS \
	Dazle_Source_Dazle_PlayerHPBarWidgetV2_h_15_INCLASS \
	Dazle_Source_Dazle_PlayerHPBarWidgetV2_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_PlayerHPBarWidgetV2_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_PlayerHPBarWidgetV2_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_PlayerHPBarWidgetV2_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_PlayerHPBarWidgetV2_h_15_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_PlayerHPBarWidgetV2_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_PlayerHPBarWidgetV2_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
