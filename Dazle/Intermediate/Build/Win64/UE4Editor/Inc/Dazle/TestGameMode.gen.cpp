// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Dazle/TestGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTestGameMode() {}
// Cross Module References
	DAZLE_API UClass* Z_Construct_UClass_ATestGameMode_NoRegister();
	DAZLE_API UClass* Z_Construct_UClass_ATestGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Dazle();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	UMG_API UClass* Z_Construct_UClass_UUserWidget_NoRegister();
// End Cross Module References
	void ATestGameMode::StaticRegisterNativesATestGameMode()
	{
	}
	UClass* Z_Construct_UClass_ATestGameMode_NoRegister()
	{
		return ATestGameMode::StaticClass();
	}
	struct Z_Construct_UClass_ATestGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_inGameWidgetClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_inGameWidgetClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_inGameWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_inGameWidget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATestGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Dazle,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATestGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "TestGameMode.h" },
		{ "ModuleRelativePath", "TestGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATestGameMode_Statics::NewProp_inGameWidgetClass_MetaData[] = {
		{ "Category", "UI" },
		{ "ModuleRelativePath", "TestGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ATestGameMode_Statics::NewProp_inGameWidgetClass = { UE4CodeGen_Private::EPropertyClass::Class, "inGameWidgetClass", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0024080000000015, 1, nullptr, STRUCT_OFFSET(ATestGameMode, inGameWidgetClass), Z_Construct_UClass_UUserWidget_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ATestGameMode_Statics::NewProp_inGameWidgetClass_MetaData, ARRAY_COUNT(Z_Construct_UClass_ATestGameMode_Statics::NewProp_inGameWidgetClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATestGameMode_Statics::NewProp_inGameWidget_MetaData[] = {
		{ "Category", "UI" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "TestGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATestGameMode_Statics::NewProp_inGameWidget = { UE4CodeGen_Private::EPropertyClass::Object, "inGameWidget", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x002008000008001d, 1, nullptr, STRUCT_OFFSET(ATestGameMode, inGameWidget), Z_Construct_UClass_UUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATestGameMode_Statics::NewProp_inGameWidget_MetaData, ARRAY_COUNT(Z_Construct_UClass_ATestGameMode_Statics::NewProp_inGameWidget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ATestGameMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATestGameMode_Statics::NewProp_inGameWidgetClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATestGameMode_Statics::NewProp_inGameWidget,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATestGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATestGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATestGameMode_Statics::ClassParams = {
		&ATestGameMode::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009002A8u,
		nullptr, 0,
		Z_Construct_UClass_ATestGameMode_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ATestGameMode_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ATestGameMode_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ATestGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATestGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATestGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATestGameMode, 2446259623);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATestGameMode(Z_Construct_UClass_ATestGameMode, &ATestGameMode::StaticClass, TEXT("/Script/Dazle"), TEXT("ATestGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATestGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
