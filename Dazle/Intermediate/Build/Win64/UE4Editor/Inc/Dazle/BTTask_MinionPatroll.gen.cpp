// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Dazle/BTTask_MinionPatroll.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBTTask_MinionPatroll() {}
// Cross Module References
	DAZLE_API UClass* Z_Construct_UClass_UBTTask_MinionPatroll_NoRegister();
	DAZLE_API UClass* Z_Construct_UClass_UBTTask_MinionPatroll();
	AIMODULE_API UClass* Z_Construct_UClass_UBTTaskNode();
	UPackage* Z_Construct_UPackage__Script_Dazle();
// End Cross Module References
	void UBTTask_MinionPatroll::StaticRegisterNativesUBTTask_MinionPatroll()
	{
	}
	UClass* Z_Construct_UClass_UBTTask_MinionPatroll_NoRegister()
	{
		return UBTTask_MinionPatroll::StaticClass();
	}
	struct Z_Construct_UClass_UBTTask_MinionPatroll_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBTTask_MinionPatroll_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBTTaskNode,
		(UObject* (*)())Z_Construct_UPackage__Script_Dazle,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBTTask_MinionPatroll_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BTTask_MinionPatroll.h" },
		{ "ModuleRelativePath", "BTTask_MinionPatroll.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBTTask_MinionPatroll_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBTTask_MinionPatroll>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBTTask_MinionPatroll_Statics::ClassParams = {
		&UBTTask_MinionPatroll::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x001000A0u,
		nullptr, 0,
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UBTTask_MinionPatroll_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UBTTask_MinionPatroll_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBTTask_MinionPatroll()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBTTask_MinionPatroll_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBTTask_MinionPatroll, 3041542184);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBTTask_MinionPatroll(Z_Construct_UClass_UBTTask_MinionPatroll, &UBTTask_MinionPatroll::StaticClass, TEXT("/Script/Dazle"), TEXT("UBTTask_MinionPatroll"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBTTask_MinionPatroll);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
