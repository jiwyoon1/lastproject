// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Dazle/ShareAIController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeShareAIController() {}
// Cross Module References
	DAZLE_API UClass* Z_Construct_UClass_AShareAIController_NoRegister();
	DAZLE_API UClass* Z_Construct_UClass_AShareAIController();
	AIMODULE_API UClass* Z_Construct_UClass_AAIController();
	UPackage* Z_Construct_UPackage__Script_Dazle();
	AIMODULE_API UClass* Z_Construct_UClass_UBlackboardData_NoRegister();
	AIMODULE_API UClass* Z_Construct_UClass_UBehaviorTree_NoRegister();
// End Cross Module References
	void AShareAIController::StaticRegisterNativesAShareAIController()
	{
	}
	UClass* Z_Construct_UClass_AShareAIController_NoRegister()
	{
		return AShareAIController::StaticClass();
	}
	struct Z_Construct_UClass_AShareAIController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_pBBData_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_pBBData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_pBTree_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_pBTree;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AShareAIController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AAIController,
		(UObject* (*)())Z_Construct_UPackage__Script_Dazle,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShareAIController_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "ShareAIController.h" },
		{ "ModuleRelativePath", "ShareAIController.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShareAIController_Statics::NewProp_pBBData_MetaData[] = {
		{ "ModuleRelativePath", "ShareAIController.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShareAIController_Statics::NewProp_pBBData = { UE4CodeGen_Private::EPropertyClass::Object, "pBBData", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000000, 1, nullptr, STRUCT_OFFSET(AShareAIController, pBBData), Z_Construct_UClass_UBlackboardData_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShareAIController_Statics::NewProp_pBBData_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShareAIController_Statics::NewProp_pBBData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShareAIController_Statics::NewProp_pBTree_MetaData[] = {
		{ "ModuleRelativePath", "ShareAIController.h" },
		{ "ToolTip", "???\xe6\xbc\xb1??" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShareAIController_Statics::NewProp_pBTree = { UE4CodeGen_Private::EPropertyClass::Object, "pBTree", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000000, 1, nullptr, STRUCT_OFFSET(AShareAIController, pBTree), Z_Construct_UClass_UBehaviorTree_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShareAIController_Statics::NewProp_pBTree_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShareAIController_Statics::NewProp_pBTree_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AShareAIController_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShareAIController_Statics::NewProp_pBBData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShareAIController_Statics::NewProp_pBTree,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AShareAIController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AShareAIController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AShareAIController_Statics::ClassParams = {
		&AShareAIController::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009002A0u,
		nullptr, 0,
		Z_Construct_UClass_AShareAIController_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_AShareAIController_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AShareAIController_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AShareAIController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AShareAIController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AShareAIController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AShareAIController, 1336329480);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AShareAIController(Z_Construct_UClass_AShareAIController, &AShareAIController::StaticClass, TEXT("/Script/Dazle"), TEXT("AShareAIController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AShareAIController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
