// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_ButtonActor_generated_h
#error "ButtonActor.generated.h already included, missing '#pragma once' in ButtonActor.h"
#endif
#define DAZLE_ButtonActor_generated_h

#define Dazle_Source_Dazle_ButtonActor_h_12_RPC_WRAPPERS
#define Dazle_Source_Dazle_ButtonActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Dazle_Source_Dazle_ButtonActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAButtonActor(); \
	friend struct Z_Construct_UClass_AButtonActor_Statics; \
public: \
	DECLARE_CLASS(AButtonActor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(AButtonActor)


#define Dazle_Source_Dazle_ButtonActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAButtonActor(); \
	friend struct Z_Construct_UClass_AButtonActor_Statics; \
public: \
	DECLARE_CLASS(AButtonActor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(AButtonActor)


#define Dazle_Source_Dazle_ButtonActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AButtonActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AButtonActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AButtonActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AButtonActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AButtonActor(AButtonActor&&); \
	NO_API AButtonActor(const AButtonActor&); \
public:


#define Dazle_Source_Dazle_ButtonActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AButtonActor(AButtonActor&&); \
	NO_API AButtonActor(const AButtonActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AButtonActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AButtonActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AButtonActor)


#define Dazle_Source_Dazle_ButtonActor_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ButtonWidgetComponent() { return STRUCT_OFFSET(AButtonActor, ButtonWidgetComponent); } \
	FORCEINLINE static uint32 __PPO__ButtonWidget() { return STRUCT_OFFSET(AButtonActor, ButtonWidget); }


#define Dazle_Source_Dazle_ButtonActor_h_9_PROLOG
#define Dazle_Source_Dazle_ButtonActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_ButtonActor_h_12_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_ButtonActor_h_12_RPC_WRAPPERS \
	Dazle_Source_Dazle_ButtonActor_h_12_INCLASS \
	Dazle_Source_Dazle_ButtonActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_ButtonActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_ButtonActor_h_12_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_ButtonActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_ButtonActor_h_12_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_ButtonActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_ButtonActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
