// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_BTService_TargetDetect_generated_h
#error "BTService_TargetDetect.generated.h already included, missing '#pragma once' in BTService_TargetDetect.h"
#endif
#define DAZLE_BTService_TargetDetect_generated_h

#define Dazle_Source_Dazle_BTService_TargetDetect_h_15_RPC_WRAPPERS
#define Dazle_Source_Dazle_BTService_TargetDetect_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Dazle_Source_Dazle_BTService_TargetDetect_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBTService_TargetDetect(); \
	friend struct Z_Construct_UClass_UBTService_TargetDetect_Statics; \
public: \
	DECLARE_CLASS(UBTService_TargetDetect, UBTService, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UBTService_TargetDetect)


#define Dazle_Source_Dazle_BTService_TargetDetect_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUBTService_TargetDetect(); \
	friend struct Z_Construct_UClass_UBTService_TargetDetect_Statics; \
public: \
	DECLARE_CLASS(UBTService_TargetDetect, UBTService, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UBTService_TargetDetect)


#define Dazle_Source_Dazle_BTService_TargetDetect_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTService_TargetDetect(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTService_TargetDetect) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTService_TargetDetect); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTService_TargetDetect); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTService_TargetDetect(UBTService_TargetDetect&&); \
	NO_API UBTService_TargetDetect(const UBTService_TargetDetect&); \
public:


#define Dazle_Source_Dazle_BTService_TargetDetect_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTService_TargetDetect(UBTService_TargetDetect&&); \
	NO_API UBTService_TargetDetect(const UBTService_TargetDetect&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTService_TargetDetect); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTService_TargetDetect); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UBTService_TargetDetect)


#define Dazle_Source_Dazle_BTService_TargetDetect_h_15_PRIVATE_PROPERTY_OFFSET
#define Dazle_Source_Dazle_BTService_TargetDetect_h_12_PROLOG
#define Dazle_Source_Dazle_BTService_TargetDetect_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_BTService_TargetDetect_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_BTService_TargetDetect_h_15_RPC_WRAPPERS \
	Dazle_Source_Dazle_BTService_TargetDetect_h_15_INCLASS \
	Dazle_Source_Dazle_BTService_TargetDetect_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_BTService_TargetDetect_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_BTService_TargetDetect_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_BTService_TargetDetect_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_BTService_TargetDetect_h_15_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_BTService_TargetDetect_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_BTService_TargetDetect_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
