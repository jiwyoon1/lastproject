// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_PlayerAnimInstance_generated_h
#error "PlayerAnimInstance.generated.h already included, missing '#pragma once' in PlayerAnimInstance.h"
#endif
#define DAZLE_PlayerAnimInstance_generated_h

#define Dazle_Source_Dazle_PlayerAnimInstance_h_26_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAnimNotify_Dash_End) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Dash_End(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Fire_Start) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Fire_Start(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Dead_End) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Dead_End(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Hit_End) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Hit_End(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Attack_End) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Attack_End(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Attack_Start) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Attack_Start(); \
		P_NATIVE_END; \
	}


#define Dazle_Source_Dazle_PlayerAnimInstance_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAnimNotify_Dash_End) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Dash_End(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Fire_Start) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Fire_Start(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Dead_End) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Dead_End(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Hit_End) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Hit_End(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Attack_End) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Attack_End(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Attack_Start) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Attack_Start(); \
		P_NATIVE_END; \
	}


#define Dazle_Source_Dazle_PlayerAnimInstance_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPlayerAnimInstance(); \
	friend struct Z_Construct_UClass_UPlayerAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UPlayerAnimInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UPlayerAnimInstance)


#define Dazle_Source_Dazle_PlayerAnimInstance_h_26_INCLASS \
private: \
	static void StaticRegisterNativesUPlayerAnimInstance(); \
	friend struct Z_Construct_UClass_UPlayerAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UPlayerAnimInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UPlayerAnimInstance)


#define Dazle_Source_Dazle_PlayerAnimInstance_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlayerAnimInstance(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlayerAnimInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlayerAnimInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlayerAnimInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlayerAnimInstance(UPlayerAnimInstance&&); \
	NO_API UPlayerAnimInstance(const UPlayerAnimInstance&); \
public:


#define Dazle_Source_Dazle_PlayerAnimInstance_h_26_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlayerAnimInstance(UPlayerAnimInstance&&); \
	NO_API UPlayerAnimInstance(const UPlayerAnimInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlayerAnimInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlayerAnimInstance); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPlayerAnimInstance)


#define Dazle_Source_Dazle_PlayerAnimInstance_h_26_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bAttack() { return STRUCT_OFFSET(UPlayerAnimInstance, bAttack); } \
	FORCEINLINE static uint32 __PPO__bRangeAttack() { return STRUCT_OFFSET(UPlayerAnimInstance, bRangeAttack); } \
	FORCEINLINE static uint32 __PPO__bIdle() { return STRUCT_OFFSET(UPlayerAnimInstance, bIdle); } \
	FORCEINLINE static uint32 __PPO__characterSpeed() { return STRUCT_OFFSET(UPlayerAnimInstance, characterSpeed); } \
	FORCEINLINE static uint32 __PPO__characterDirection() { return STRUCT_OFFSET(UPlayerAnimInstance, characterDirection); } \
	FORCEINLINE static uint32 __PPO__fWalkRate() { return STRUCT_OFFSET(UPlayerAnimInstance, fWalkRate); } \
	FORCEINLINE static uint32 __PPO__iBaseAnimState() { return STRUCT_OFFSET(UPlayerAnimInstance, iBaseAnimState); } \
	FORCEINLINE static uint32 __PPO__fVelocity() { return STRUCT_OFFSET(UPlayerAnimInstance, fVelocity); } \
	FORCEINLINE static uint32 __PPO__bJump() { return STRUCT_OFFSET(UPlayerAnimInstance, bJump); } \
	FORCEINLINE static uint32 __PPO__attackswich() { return STRUCT_OFFSET(UPlayerAnimInstance, attackswich); } \
	FORCEINLINE static uint32 __PPO__bDamage() { return STRUCT_OFFSET(UPlayerAnimInstance, bDamage); } \
	FORCEINLINE static uint32 __PPO__bDead() { return STRUCT_OFFSET(UPlayerAnimInstance, bDead); } \
	FORCEINLINE static uint32 __PPO__bDash() { return STRUCT_OFFSET(UPlayerAnimInstance, bDash); } \
	FORCEINLINE static uint32 __PPO__fPlayerPitch() { return STRUCT_OFFSET(UPlayerAnimInstance, fPlayerPitch); } \
	FORCEINLINE static uint32 __PPO__fPlayerYaw() { return STRUCT_OFFSET(UPlayerAnimInstance, fPlayerYaw); }


#define Dazle_Source_Dazle_PlayerAnimInstance_h_23_PROLOG
#define Dazle_Source_Dazle_PlayerAnimInstance_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_PlayerAnimInstance_h_26_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_PlayerAnimInstance_h_26_RPC_WRAPPERS \
	Dazle_Source_Dazle_PlayerAnimInstance_h_26_INCLASS \
	Dazle_Source_Dazle_PlayerAnimInstance_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_PlayerAnimInstance_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_PlayerAnimInstance_h_26_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_PlayerAnimInstance_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_PlayerAnimInstance_h_26_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_PlayerAnimInstance_h_26_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_PlayerAnimInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
