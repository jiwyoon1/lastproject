// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Dazle/IngameWidget.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIngameWidget() {}
// Cross Module References
	DAZLE_API UClass* Z_Construct_UClass_UIngameWidget_NoRegister();
	DAZLE_API UClass* Z_Construct_UClass_UIngameWidget();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_Dazle();
	DAZLE_API UFunction* Z_Construct_UFunction_UIngameWidget_EmptyTestBtn();
	UMG_API UClass* Z_Construct_UClass_UProgressBar_NoRegister();
// End Cross Module References
	void UIngameWidget::StaticRegisterNativesUIngameWidget()
	{
		UClass* Class = UIngameWidget::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "EmptyTestBtn", &UIngameWidget::execEmptyTestBtn },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UIngameWidget_EmptyTestBtn_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UIngameWidget_EmptyTestBtn_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "IngameWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UIngameWidget_EmptyTestBtn_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UIngameWidget, "EmptyTestBtn", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UIngameWidget_EmptyTestBtn_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UIngameWidget_EmptyTestBtn_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UIngameWidget_EmptyTestBtn()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UIngameWidget_EmptyTestBtn_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UIngameWidget_NoRegister()
	{
		return UIngameWidget::StaticClass();
	}
	struct Z_Construct_UClass_UIngameWidget_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HpBar_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HpBar;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_fValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_m_fValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UIngameWidget_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_Dazle,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UIngameWidget_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UIngameWidget_EmptyTestBtn, "EmptyTestBtn" }, // 3915029136
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIngameWidget_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "IngameWidget.h" },
		{ "ModuleRelativePath", "IngameWidget.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIngameWidget_Statics::NewProp_HpBar_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "IngameWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UIngameWidget_Statics::NewProp_HpBar = { UE4CodeGen_Private::EPropertyClass::Object, "HpBar", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000080008, 1, nullptr, STRUCT_OFFSET(UIngameWidget, HpBar), Z_Construct_UClass_UProgressBar_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UIngameWidget_Statics::NewProp_HpBar_MetaData, ARRAY_COUNT(Z_Construct_UClass_UIngameWidget_Statics::NewProp_HpBar_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UIngameWidget_Statics::NewProp_m_fValue_MetaData[] = {
		{ "ModuleRelativePath", "IngameWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UIngameWidget_Statics::NewProp_m_fValue = { UE4CodeGen_Private::EPropertyClass::Float, "m_fValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000000, 1, nullptr, STRUCT_OFFSET(UIngameWidget, m_fValue), METADATA_PARAMS(Z_Construct_UClass_UIngameWidget_Statics::NewProp_m_fValue_MetaData, ARRAY_COUNT(Z_Construct_UClass_UIngameWidget_Statics::NewProp_m_fValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UIngameWidget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIngameWidget_Statics::NewProp_HpBar,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UIngameWidget_Statics::NewProp_m_fValue,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UIngameWidget_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UIngameWidget>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UIngameWidget_Statics::ClassParams = {
		&UIngameWidget::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x00B010A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_UIngameWidget_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_UIngameWidget_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UIngameWidget_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UIngameWidget_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UIngameWidget()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UIngameWidget_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UIngameWidget, 448687257);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UIngameWidget(Z_Construct_UClass_UIngameWidget, &UIngameWidget::StaticClass, TEXT("/Script/Dazle"), TEXT("UIngameWidget"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UIngameWidget);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
