// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_JombieAnimInstance_generated_h
#error "JombieAnimInstance.generated.h already included, missing '#pragma once' in JombieAnimInstance.h"
#endif
#define DAZLE_JombieAnimInstance_generated_h

#define Dazle_Source_Dazle_JombieAnimInstance_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAnimNotify_Dead_End) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Dead_End(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Hit_End) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Hit_End(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Attack_End) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Attack_End(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Attack_Start) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Attack_Start(); \
		P_NATIVE_END; \
	}


#define Dazle_Source_Dazle_JombieAnimInstance_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAnimNotify_Dead_End) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Dead_End(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Hit_End) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Hit_End(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Attack_End) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Attack_End(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAnimNotify_Attack_Start) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AnimNotify_Attack_Start(); \
		P_NATIVE_END; \
	}


#define Dazle_Source_Dazle_JombieAnimInstance_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUJombieAnimInstance(); \
	friend struct Z_Construct_UClass_UJombieAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UJombieAnimInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UJombieAnimInstance)


#define Dazle_Source_Dazle_JombieAnimInstance_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUJombieAnimInstance(); \
	friend struct Z_Construct_UClass_UJombieAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UJombieAnimInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UJombieAnimInstance)


#define Dazle_Source_Dazle_JombieAnimInstance_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UJombieAnimInstance(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UJombieAnimInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UJombieAnimInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UJombieAnimInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UJombieAnimInstance(UJombieAnimInstance&&); \
	NO_API UJombieAnimInstance(const UJombieAnimInstance&); \
public:


#define Dazle_Source_Dazle_JombieAnimInstance_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UJombieAnimInstance(UJombieAnimInstance&&); \
	NO_API UJombieAnimInstance(const UJombieAnimInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UJombieAnimInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UJombieAnimInstance); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UJombieAnimInstance)


#define Dazle_Source_Dazle_JombieAnimInstance_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bAttack() { return STRUCT_OFFSET(UJombieAnimInstance, bAttack); } \
	FORCEINLINE static uint32 __PPO__bIdle() { return STRUCT_OFFSET(UJombieAnimInstance, bIdle); } \
	FORCEINLINE static uint32 __PPO__characterSpeed() { return STRUCT_OFFSET(UJombieAnimInstance, characterSpeed); } \
	FORCEINLINE static uint32 __PPO__characterDirection() { return STRUCT_OFFSET(UJombieAnimInstance, characterDirection); } \
	FORCEINLINE static uint32 __PPO__iBaseAnimState() { return STRUCT_OFFSET(UJombieAnimInstance, iBaseAnimState); } \
	FORCEINLINE static uint32 __PPO__fVelocity() { return STRUCT_OFFSET(UJombieAnimInstance, fVelocity); } \
	FORCEINLINE static uint32 __PPO__bJump() { return STRUCT_OFFSET(UJombieAnimInstance, bJump); } \
	FORCEINLINE static uint32 __PPO__attackswich() { return STRUCT_OFFSET(UJombieAnimInstance, attackswich); } \
	FORCEINLINE static uint32 __PPO__bDamage() { return STRUCT_OFFSET(UJombieAnimInstance, bDamage); } \
	FORCEINLINE static uint32 __PPO__bDead() { return STRUCT_OFFSET(UJombieAnimInstance, bDead); }


#define Dazle_Source_Dazle_JombieAnimInstance_h_12_PROLOG
#define Dazle_Source_Dazle_JombieAnimInstance_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_JombieAnimInstance_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_JombieAnimInstance_h_15_RPC_WRAPPERS \
	Dazle_Source_Dazle_JombieAnimInstance_h_15_INCLASS \
	Dazle_Source_Dazle_JombieAnimInstance_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_JombieAnimInstance_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_JombieAnimInstance_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_JombieAnimInstance_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_JombieAnimInstance_h_15_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_JombieAnimInstance_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_JombieAnimInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
