// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_DazleGameModeBase_generated_h
#error "DazleGameModeBase.generated.h already included, missing '#pragma once' in DazleGameModeBase.h"
#endif
#define DAZLE_DazleGameModeBase_generated_h

#define Dazle_Source_Dazle_DazleGameModeBase_h_15_RPC_WRAPPERS
#define Dazle_Source_Dazle_DazleGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Dazle_Source_Dazle_DazleGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADazleGameModeBase(); \
	friend struct Z_Construct_UClass_ADazleGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ADazleGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(ADazleGameModeBase)


#define Dazle_Source_Dazle_DazleGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesADazleGameModeBase(); \
	friend struct Z_Construct_UClass_ADazleGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ADazleGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(ADazleGameModeBase)


#define Dazle_Source_Dazle_DazleGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADazleGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADazleGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADazleGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADazleGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADazleGameModeBase(ADazleGameModeBase&&); \
	NO_API ADazleGameModeBase(const ADazleGameModeBase&); \
public:


#define Dazle_Source_Dazle_DazleGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADazleGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADazleGameModeBase(ADazleGameModeBase&&); \
	NO_API ADazleGameModeBase(const ADazleGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADazleGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADazleGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADazleGameModeBase)


#define Dazle_Source_Dazle_DazleGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Dazle_Source_Dazle_DazleGameModeBase_h_12_PROLOG
#define Dazle_Source_Dazle_DazleGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_DazleGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_DazleGameModeBase_h_15_RPC_WRAPPERS \
	Dazle_Source_Dazle_DazleGameModeBase_h_15_INCLASS \
	Dazle_Source_Dazle_DazleGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_DazleGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_DazleGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_DazleGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_DazleGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_DazleGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_DazleGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
