// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_ShareAIController_generated_h
#error "ShareAIController.generated.h already included, missing '#pragma once' in ShareAIController.h"
#endif
#define DAZLE_ShareAIController_generated_h

#define Dazle_Source_Dazle_ShareAIController_h_15_RPC_WRAPPERS
#define Dazle_Source_Dazle_ShareAIController_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Dazle_Source_Dazle_ShareAIController_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAShareAIController(); \
	friend struct Z_Construct_UClass_AShareAIController_Statics; \
public: \
	DECLARE_CLASS(AShareAIController, AAIController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(AShareAIController)


#define Dazle_Source_Dazle_ShareAIController_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAShareAIController(); \
	friend struct Z_Construct_UClass_AShareAIController_Statics; \
public: \
	DECLARE_CLASS(AShareAIController, AAIController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(AShareAIController)


#define Dazle_Source_Dazle_ShareAIController_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShareAIController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShareAIController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShareAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShareAIController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShareAIController(AShareAIController&&); \
	NO_API AShareAIController(const AShareAIController&); \
public:


#define Dazle_Source_Dazle_ShareAIController_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShareAIController(AShareAIController&&); \
	NO_API AShareAIController(const AShareAIController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShareAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShareAIController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AShareAIController)


#define Dazle_Source_Dazle_ShareAIController_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__pBTree() { return STRUCT_OFFSET(AShareAIController, pBTree); } \
	FORCEINLINE static uint32 __PPO__pBBData() { return STRUCT_OFFSET(AShareAIController, pBBData); }


#define Dazle_Source_Dazle_ShareAIController_h_12_PROLOG
#define Dazle_Source_Dazle_ShareAIController_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_ShareAIController_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_ShareAIController_h_15_RPC_WRAPPERS \
	Dazle_Source_Dazle_ShareAIController_h_15_INCLASS \
	Dazle_Source_Dazle_ShareAIController_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_ShareAIController_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_ShareAIController_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_ShareAIController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_ShareAIController_h_15_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_ShareAIController_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_ShareAIController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
