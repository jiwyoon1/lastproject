// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_BTTaskNode_Attack_generated_h
#error "BTTaskNode_Attack.generated.h already included, missing '#pragma once' in BTTaskNode_Attack.h"
#endif
#define DAZLE_BTTaskNode_Attack_generated_h

#define Dazle_Source_Dazle_BTTaskNode_Attack_h_15_RPC_WRAPPERS
#define Dazle_Source_Dazle_BTTaskNode_Attack_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Dazle_Source_Dazle_BTTaskNode_Attack_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBTTaskNode_Attack(); \
	friend struct Z_Construct_UClass_UBTTaskNode_Attack_Statics; \
public: \
	DECLARE_CLASS(UBTTaskNode_Attack, UBTTaskNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UBTTaskNode_Attack)


#define Dazle_Source_Dazle_BTTaskNode_Attack_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUBTTaskNode_Attack(); \
	friend struct Z_Construct_UClass_UBTTaskNode_Attack_Statics; \
public: \
	DECLARE_CLASS(UBTTaskNode_Attack, UBTTaskNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UBTTaskNode_Attack)


#define Dazle_Source_Dazle_BTTaskNode_Attack_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTTaskNode_Attack(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTTaskNode_Attack) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTaskNode_Attack); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTaskNode_Attack); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTaskNode_Attack(UBTTaskNode_Attack&&); \
	NO_API UBTTaskNode_Attack(const UBTTaskNode_Attack&); \
public:


#define Dazle_Source_Dazle_BTTaskNode_Attack_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTaskNode_Attack(UBTTaskNode_Attack&&); \
	NO_API UBTTaskNode_Attack(const UBTTaskNode_Attack&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTaskNode_Attack); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTaskNode_Attack); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UBTTaskNode_Attack)


#define Dazle_Source_Dazle_BTTaskNode_Attack_h_15_PRIVATE_PROPERTY_OFFSET
#define Dazle_Source_Dazle_BTTaskNode_Attack_h_12_PROLOG
#define Dazle_Source_Dazle_BTTaskNode_Attack_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_BTTaskNode_Attack_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_BTTaskNode_Attack_h_15_RPC_WRAPPERS \
	Dazle_Source_Dazle_BTTaskNode_Attack_h_15_INCLASS \
	Dazle_Source_Dazle_BTTaskNode_Attack_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_BTTaskNode_Attack_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_BTTaskNode_Attack_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_BTTaskNode_Attack_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_BTTaskNode_Attack_h_15_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_BTTaskNode_Attack_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_BTTaskNode_Attack_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
