// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Dazle/ButtonWidget.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeButtonWidget() {}
// Cross Module References
	DAZLE_API UClass* Z_Construct_UClass_UButtonWidget_NoRegister();
	DAZLE_API UClass* Z_Construct_UClass_UButtonWidget();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_Dazle();
// End Cross Module References
	void UButtonWidget::StaticRegisterNativesUButtonWidget()
	{
	}
	UClass* Z_Construct_UClass_UButtonWidget_NoRegister()
	{
		return UButtonWidget::StaticClass();
	}
	struct Z_Construct_UClass_UButtonWidget_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UButtonWidget_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_Dazle,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UButtonWidget_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ButtonWidget.h" },
		{ "ModuleRelativePath", "ButtonWidget.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UButtonWidget_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UButtonWidget>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UButtonWidget_Statics::ClassParams = {
		&UButtonWidget::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x00B010A0u,
		nullptr, 0,
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UButtonWidget_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UButtonWidget_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UButtonWidget()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UButtonWidget_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UButtonWidget, 1792161011);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UButtonWidget(Z_Construct_UClass_UButtonWidget, &UButtonWidget::StaticClass, TEXT("/Script/Dazle"), TEXT("UButtonWidget"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UButtonWidget);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
