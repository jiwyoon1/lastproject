// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Dazle/JombieAnimInstance.h"
#include "Engine/Classes/Components/SkeletalMeshComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeJombieAnimInstance() {}
// Cross Module References
	DAZLE_API UClass* Z_Construct_UClass_UJombieAnimInstance_NoRegister();
	DAZLE_API UClass* Z_Construct_UClass_UJombieAnimInstance();
	ENGINE_API UClass* Z_Construct_UClass_UAnimInstance();
	UPackage* Z_Construct_UPackage__Script_Dazle();
	DAZLE_API UFunction* Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Attack_End();
	DAZLE_API UFunction* Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Attack_Start();
	DAZLE_API UFunction* Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Dead_End();
	DAZLE_API UFunction* Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Hit_End();
// End Cross Module References
	void UJombieAnimInstance::StaticRegisterNativesUJombieAnimInstance()
	{
		UClass* Class = UJombieAnimInstance::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AnimNotify_Attack_End", &UJombieAnimInstance::execAnimNotify_Attack_End },
			{ "AnimNotify_Attack_Start", &UJombieAnimInstance::execAnimNotify_Attack_Start },
			{ "AnimNotify_Dead_End", &UJombieAnimInstance::execAnimNotify_Dead_End },
			{ "AnimNotify_Hit_End", &UJombieAnimInstance::execAnimNotify_Hit_End },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Attack_End_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Attack_End_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "JombieAnimInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Attack_End_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UJombieAnimInstance, "AnimNotify_Attack_End", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00040401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Attack_End_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Attack_End_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Attack_End()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Attack_End_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Attack_Start_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Attack_Start_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "JombieAnimInstance.h" },
		{ "ToolTip", "ANIMATION_TYPE m_eAnimType;" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Attack_Start_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UJombieAnimInstance, "AnimNotify_Attack_Start", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00040401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Attack_Start_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Attack_Start_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Attack_Start()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Attack_Start_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Dead_End_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Dead_End_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "JombieAnimInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Dead_End_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UJombieAnimInstance, "AnimNotify_Dead_End", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00040401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Dead_End_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Dead_End_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Dead_End()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Dead_End_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Hit_End_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Hit_End_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "JombieAnimInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Hit_End_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UJombieAnimInstance, "AnimNotify_Hit_End", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00040401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Hit_End_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Hit_End_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Hit_End()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Hit_End_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UJombieAnimInstance_NoRegister()
	{
		return UJombieAnimInstance::StaticClass();
	}
	struct Z_Construct_UClass_UJombieAnimInstance_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDead_MetaData[];
#endif
		static void NewProp_bDead_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDead;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDamage_MetaData[];
#endif
		static void NewProp_bDamage_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDamage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_attackswich_MetaData[];
#endif
		static void NewProp_attackswich_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_attackswich;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bJump_MetaData[];
#endif
		static void NewProp_bJump_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bJump;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fVelocity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_fVelocity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_iBaseAnimState_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_iBaseAnimState;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_characterDirection_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_characterDirection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_characterSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_characterSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIdle_MetaData[];
#endif
		static void NewProp_bIdle_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIdle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAttack_MetaData[];
#endif
		static void NewProp_bAttack_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAttack;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UJombieAnimInstance_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAnimInstance,
		(UObject* (*)())Z_Construct_UPackage__Script_Dazle,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UJombieAnimInstance_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Attack_End, "AnimNotify_Attack_End" }, // 3948439869
		{ &Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Attack_Start, "AnimNotify_Attack_Start" }, // 4101465533
		{ &Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Dead_End, "AnimNotify_Dead_End" }, // 3056534022
		{ &Z_Construct_UFunction_UJombieAnimInstance_AnimNotify_Hit_End, "AnimNotify_Hit_End" }, // 1384657815
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UJombieAnimInstance_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "AnimInstance" },
		{ "IncludePath", "JombieAnimInstance.h" },
		{ "ModuleRelativePath", "JombieAnimInstance.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bDead_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Pawn" },
		{ "ModuleRelativePath", "JombieAnimInstance.h" },
	};
#endif
	void Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bDead_SetBit(void* Obj)
	{
		((UJombieAnimInstance*)Obj)->bDead = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bDead = { UE4CodeGen_Private::EPropertyClass::Bool, "bDead", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(UJombieAnimInstance), &Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bDead_SetBit, METADATA_PARAMS(Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bDead_MetaData, ARRAY_COUNT(Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bDead_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bDamage_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Pawn" },
		{ "ModuleRelativePath", "JombieAnimInstance.h" },
	};
#endif
	void Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bDamage_SetBit(void* Obj)
	{
		((UJombieAnimInstance*)Obj)->bDamage = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bDamage = { UE4CodeGen_Private::EPropertyClass::Bool, "bDamage", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(UJombieAnimInstance), &Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bDamage_SetBit, METADATA_PARAMS(Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bDamage_MetaData, ARRAY_COUNT(Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bDamage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_attackswich_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Pawn" },
		{ "ModuleRelativePath", "JombieAnimInstance.h" },
	};
#endif
	void Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_attackswich_SetBit(void* Obj)
	{
		((UJombieAnimInstance*)Obj)->attackswich = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_attackswich = { UE4CodeGen_Private::EPropertyClass::Bool, "attackswich", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(UJombieAnimInstance), &Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_attackswich_SetBit, METADATA_PARAMS(Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_attackswich_MetaData, ARRAY_COUNT(Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_attackswich_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bJump_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Pawn" },
		{ "ModuleRelativePath", "JombieAnimInstance.h" },
	};
#endif
	void Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bJump_SetBit(void* Obj)
	{
		((UJombieAnimInstance*)Obj)->bJump = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bJump = { UE4CodeGen_Private::EPropertyClass::Bool, "bJump", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(UJombieAnimInstance), &Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bJump_SetBit, METADATA_PARAMS(Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bJump_MetaData, ARRAY_COUNT(Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bJump_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_fVelocity_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Pawn" },
		{ "ModuleRelativePath", "JombieAnimInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_fVelocity = { UE4CodeGen_Private::EPropertyClass::Float, "fVelocity", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000015, 1, nullptr, STRUCT_OFFSET(UJombieAnimInstance, fVelocity), METADATA_PARAMS(Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_fVelocity_MetaData, ARRAY_COUNT(Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_fVelocity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_iBaseAnimState_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Pawn" },
		{ "ModuleRelativePath", "JombieAnimInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_iBaseAnimState = { UE4CodeGen_Private::EPropertyClass::Int, "iBaseAnimState", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000015, 1, nullptr, STRUCT_OFFSET(UJombieAnimInstance, iBaseAnimState), METADATA_PARAMS(Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_iBaseAnimState_MetaData, ARRAY_COUNT(Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_iBaseAnimState_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_characterDirection_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Pawn" },
		{ "ModuleRelativePath", "JombieAnimInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_characterDirection = { UE4CodeGen_Private::EPropertyClass::Float, "characterDirection", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000005, 1, nullptr, STRUCT_OFFSET(UJombieAnimInstance, characterDirection), METADATA_PARAMS(Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_characterDirection_MetaData, ARRAY_COUNT(Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_characterDirection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_characterSpeed_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Pawn" },
		{ "ModuleRelativePath", "JombieAnimInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_characterSpeed = { UE4CodeGen_Private::EPropertyClass::Float, "characterSpeed", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000005, 1, nullptr, STRUCT_OFFSET(UJombieAnimInstance, characterSpeed), METADATA_PARAMS(Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_characterSpeed_MetaData, ARRAY_COUNT(Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_characterSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bIdle_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Pawn" },
		{ "ModuleRelativePath", "JombieAnimInstance.h" },
	};
#endif
	void Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bIdle_SetBit(void* Obj)
	{
		((UJombieAnimInstance*)Obj)->bIdle = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bIdle = { UE4CodeGen_Private::EPropertyClass::Bool, "bIdle", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(UJombieAnimInstance), &Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bIdle_SetBit, METADATA_PARAMS(Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bIdle_MetaData, ARRAY_COUNT(Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bIdle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bAttack_MetaData[] = {
		{ "AllowPrivateAccess", "TRUE" },
		{ "Category", "Pawn" },
		{ "ModuleRelativePath", "JombieAnimInstance.h" },
		{ "ToolTip", "?\xe2\xba\xbb?????? UPROPERTY?? ?\xca\xb1?\xc8\xad?\xcf\xb8? 0?\xcc\xb5\xc8\xb4?." },
	};
#endif
	void Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bAttack_SetBit(void* Obj)
	{
		((UJombieAnimInstance*)Obj)->bAttack = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bAttack = { UE4CodeGen_Private::EPropertyClass::Bool, "bAttack", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(UJombieAnimInstance), &Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bAttack_SetBit, METADATA_PARAMS(Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bAttack_MetaData, ARRAY_COUNT(Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bAttack_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UJombieAnimInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bDead,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bDamage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_attackswich,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bJump,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_fVelocity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_iBaseAnimState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_characterDirection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_characterSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bIdle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UJombieAnimInstance_Statics::NewProp_bAttack,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UJombieAnimInstance_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UJombieAnimInstance>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UJombieAnimInstance_Statics::ClassParams = {
		&UJombieAnimInstance::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A8u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_UJombieAnimInstance_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_UJombieAnimInstance_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UJombieAnimInstance_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UJombieAnimInstance_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UJombieAnimInstance()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UJombieAnimInstance_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UJombieAnimInstance, 3942301741);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UJombieAnimInstance(Z_Construct_UClass_UJombieAnimInstance, &UJombieAnimInstance::StaticClass, TEXT("/Script/Dazle"), TEXT("UJombieAnimInstance"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UJombieAnimInstance);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
