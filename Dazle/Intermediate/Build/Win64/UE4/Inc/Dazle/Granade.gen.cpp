// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Dazle/Granade.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGranade() {}
// Cross Module References
	DAZLE_API UClass* Z_Construct_UClass_AGranade_NoRegister();
	DAZLE_API UClass* Z_Construct_UClass_AGranade();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Dazle();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void AGranade::StaticRegisterNativesAGranade()
	{
	}
	UClass* Z_Construct_UClass_AGranade_NoRegister()
	{
		return AGranade::StaticClass();
	}
	struct Z_Construct_UClass_AGranade_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_mGranadeMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_mGranadeMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AGranade_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Dazle,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGranade_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Granade.h" },
		{ "ModuleRelativePath", "Granade.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGranade_Statics::NewProp_mGranadeMesh_MetaData[] = {
		{ "Category", "Granade" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Granade.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AGranade_Statics::NewProp_mGranadeMesh = { UE4CodeGen_Private::EPropertyClass::Object, "mGranadeMesh", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000080009, 1, nullptr, STRUCT_OFFSET(AGranade, mGranadeMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AGranade_Statics::NewProp_mGranadeMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGranade_Statics::NewProp_mGranadeMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AGranade_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGranade_Statics::NewProp_mGranadeMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AGranade_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AGranade>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AGranade_Statics::ClassParams = {
		&AGranade::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		nullptr, 0,
		Z_Construct_UClass_AGranade_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_AGranade_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AGranade_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AGranade_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AGranade()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AGranade_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AGranade, 2835398956);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGranade(Z_Construct_UClass_AGranade, &AGranade::StaticClass, TEXT("/Script/Dazle"), TEXT("AGranade"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGranade);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
