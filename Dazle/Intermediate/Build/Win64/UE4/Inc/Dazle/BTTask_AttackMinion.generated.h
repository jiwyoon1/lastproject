// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_BTTask_AttackMinion_generated_h
#error "BTTask_AttackMinion.generated.h already included, missing '#pragma once' in BTTask_AttackMinion.h"
#endif
#define DAZLE_BTTask_AttackMinion_generated_h

#define Dazle_Source_Dazle_BTTask_AttackMinion_h_15_RPC_WRAPPERS
#define Dazle_Source_Dazle_BTTask_AttackMinion_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Dazle_Source_Dazle_BTTask_AttackMinion_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBTTask_AttackMinion(); \
	friend struct Z_Construct_UClass_UBTTask_AttackMinion_Statics; \
public: \
	DECLARE_CLASS(UBTTask_AttackMinion, UBTTaskNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UBTTask_AttackMinion)


#define Dazle_Source_Dazle_BTTask_AttackMinion_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUBTTask_AttackMinion(); \
	friend struct Z_Construct_UClass_UBTTask_AttackMinion_Statics; \
public: \
	DECLARE_CLASS(UBTTask_AttackMinion, UBTTaskNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UBTTask_AttackMinion)


#define Dazle_Source_Dazle_BTTask_AttackMinion_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTTask_AttackMinion(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTTask_AttackMinion) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTask_AttackMinion); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTask_AttackMinion); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTask_AttackMinion(UBTTask_AttackMinion&&); \
	NO_API UBTTask_AttackMinion(const UBTTask_AttackMinion&); \
public:


#define Dazle_Source_Dazle_BTTask_AttackMinion_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTTask_AttackMinion(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTask_AttackMinion(UBTTask_AttackMinion&&); \
	NO_API UBTTask_AttackMinion(const UBTTask_AttackMinion&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTask_AttackMinion); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTask_AttackMinion); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTTask_AttackMinion)


#define Dazle_Source_Dazle_BTTask_AttackMinion_h_15_PRIVATE_PROPERTY_OFFSET
#define Dazle_Source_Dazle_BTTask_AttackMinion_h_12_PROLOG
#define Dazle_Source_Dazle_BTTask_AttackMinion_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_BTTask_AttackMinion_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_BTTask_AttackMinion_h_15_RPC_WRAPPERS \
	Dazle_Source_Dazle_BTTask_AttackMinion_h_15_INCLASS \
	Dazle_Source_Dazle_BTTask_AttackMinion_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_BTTask_AttackMinion_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_BTTask_AttackMinion_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_BTTask_AttackMinion_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_BTTask_AttackMinion_h_15_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_BTTask_AttackMinion_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_BTTask_AttackMinion_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
