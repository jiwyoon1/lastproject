// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef DAZLE_NGBullet_generated_h
#error "NGBullet.generated.h already included, missing '#pragma once' in NGBullet.h"
#endif
#define DAZLE_NGBullet_generated_h

#define Dazle_Source_Dazle_NGBullet_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_STRUCT(FVector,Z_Param_vNormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_vNormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define Dazle_Source_Dazle_NGBullet_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_STRUCT(FVector,Z_Param_vNormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_vNormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define Dazle_Source_Dazle_NGBullet_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesANGBullet(); \
	friend struct Z_Construct_UClass_ANGBullet_Statics; \
public: \
	DECLARE_CLASS(ANGBullet, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(ANGBullet)


#define Dazle_Source_Dazle_NGBullet_h_14_INCLASS \
private: \
	static void StaticRegisterNativesANGBullet(); \
	friend struct Z_Construct_UClass_ANGBullet_Statics; \
public: \
	DECLARE_CLASS(ANGBullet, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(ANGBullet)


#define Dazle_Source_Dazle_NGBullet_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANGBullet(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANGBullet) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANGBullet); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANGBullet); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANGBullet(ANGBullet&&); \
	NO_API ANGBullet(const ANGBullet&); \
public:


#define Dazle_Source_Dazle_NGBullet_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANGBullet(ANGBullet&&); \
	NO_API ANGBullet(const ANGBullet&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANGBullet); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANGBullet); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ANGBullet)


#define Dazle_Source_Dazle_NGBullet_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SphereCollision() { return STRUCT_OFFSET(ANGBullet, SphereCollision); } \
	FORCEINLINE static uint32 __PPO__BulletParticle() { return STRUCT_OFFSET(ANGBullet, BulletParticle); } \
	FORCEINLINE static uint32 __PPO__Mesh() { return STRUCT_OFFSET(ANGBullet, Mesh); } \
	FORCEINLINE static uint32 __PPO__MovementCom() { return STRUCT_OFFSET(ANGBullet, MovementCom); } \
	FORCEINLINE static uint32 __PPO__selfController() { return STRUCT_OFFSET(ANGBullet, selfController); }


#define Dazle_Source_Dazle_NGBullet_h_11_PROLOG
#define Dazle_Source_Dazle_NGBullet_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_NGBullet_h_14_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_NGBullet_h_14_RPC_WRAPPERS \
	Dazle_Source_Dazle_NGBullet_h_14_INCLASS \
	Dazle_Source_Dazle_NGBullet_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_NGBullet_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_NGBullet_h_14_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_NGBullet_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_NGBullet_h_14_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_NGBullet_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_NGBullet_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
