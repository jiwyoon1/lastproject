// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Dazle/PlayerHPWidget.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlayerHPWidget() {}
// Cross Module References
	DAZLE_API UClass* Z_Construct_UClass_UPlayerHPWidget_NoRegister();
	DAZLE_API UClass* Z_Construct_UClass_UPlayerHPWidget();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_Dazle();
	UMG_API UClass* Z_Construct_UClass_UProgressBar_NoRegister();
// End Cross Module References
	void UPlayerHPWidget::StaticRegisterNativesUPlayerHPWidget()
	{
	}
	UClass* Z_Construct_UClass_UPlayerHPWidget_NoRegister()
	{
		return UPlayerHPWidget::StaticClass();
	}
	struct Z_Construct_UClass_UPlayerHPWidget_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProgressBar_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ProgressBar;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_fValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_m_fValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPlayerHPWidget_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_Dazle,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlayerHPWidget_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PlayerHPWidget.h" },
		{ "ModuleRelativePath", "PlayerHPWidget.h" },
		{ "ToolTip", "//58?? 12??\n//??\xc6\xbc?\xc9\xbd?\xc6\xae ?????\xd2\xb3??? ?????\xcf\xb0? ?????????? ?\xd2\xb3??? ??????\xc3\xb3???? ?????\xcf\xb5??? ?????? \xc3\xb3???\xcf\xb4\xc2\xb0? ????." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlayerHPWidget_Statics::NewProp_ProgressBar_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "PlayerHPWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPlayerHPWidget_Statics::NewProp_ProgressBar = { UE4CodeGen_Private::EPropertyClass::Object, "ProgressBar", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000080008, 1, nullptr, STRUCT_OFFSET(UPlayerHPWidget, ProgressBar), Z_Construct_UClass_UProgressBar_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPlayerHPWidget_Statics::NewProp_ProgressBar_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPlayerHPWidget_Statics::NewProp_ProgressBar_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlayerHPWidget_Statics::NewProp_m_fValue_MetaData[] = {
		{ "ModuleRelativePath", "PlayerHPWidget.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPlayerHPWidget_Statics::NewProp_m_fValue = { UE4CodeGen_Private::EPropertyClass::Float, "m_fValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000000, 1, nullptr, STRUCT_OFFSET(UPlayerHPWidget, m_fValue), METADATA_PARAMS(Z_Construct_UClass_UPlayerHPWidget_Statics::NewProp_m_fValue_MetaData, ARRAY_COUNT(Z_Construct_UClass_UPlayerHPWidget_Statics::NewProp_m_fValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPlayerHPWidget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlayerHPWidget_Statics::NewProp_ProgressBar,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlayerHPWidget_Statics::NewProp_m_fValue,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPlayerHPWidget_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPlayerHPWidget>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPlayerHPWidget_Statics::ClassParams = {
		&UPlayerHPWidget::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x00B010A0u,
		nullptr, 0,
		Z_Construct_UClass_UPlayerHPWidget_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_UPlayerHPWidget_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UPlayerHPWidget_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UPlayerHPWidget_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPlayerHPWidget()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPlayerHPWidget_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPlayerHPWidget, 2939361916);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPlayerHPWidget(Z_Construct_UClass_UPlayerHPWidget, &UPlayerHPWidget::StaticClass, TEXT("/Script/Dazle"), TEXT("UPlayerHPWidget"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPlayerHPWidget);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
