// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_PlayerCharacter_generated_h
#error "PlayerCharacter.generated.h already included, missing '#pragma once' in PlayerCharacter.h"
#endif
#define DAZLE_PlayerCharacter_generated_h

#define Dazle_Source_Dazle_PlayerCharacter_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetDashCollTime) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetDashCollTime(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetItemSlotSecond) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetItemSlotSecond(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetItemSlotFirst) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetItemSlotFirst(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetPercentHp) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->getPercentHp(); \
		P_NATIVE_END; \
	}


#define Dazle_Source_Dazle_PlayerCharacter_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetDashCollTime) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetDashCollTime(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetItemSlotSecond) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetItemSlotSecond(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetItemSlotFirst) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetItemSlotFirst(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetPercentHp) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->getPercentHp(); \
		P_NATIVE_END; \
	}


#define Dazle_Source_Dazle_PlayerCharacter_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerCharacter(); \
	friend struct Z_Construct_UClass_APlayerCharacter_Statics; \
public: \
	DECLARE_CLASS(APlayerCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(APlayerCharacter)


#define Dazle_Source_Dazle_PlayerCharacter_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerCharacter(); \
	friend struct Z_Construct_UClass_APlayerCharacter_Statics; \
public: \
	DECLARE_CLASS(APlayerCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(APlayerCharacter)


#define Dazle_Source_Dazle_PlayerCharacter_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerCharacter(APlayerCharacter&&); \
	NO_API APlayerCharacter(const APlayerCharacter&); \
public:


#define Dazle_Source_Dazle_PlayerCharacter_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerCharacter(APlayerCharacter&&); \
	NO_API APlayerCharacter(const APlayerCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerCharacter)


#define Dazle_Source_Dazle_PlayerCharacter_h_16_PRIVATE_PROPERTY_OFFSET
#define Dazle_Source_Dazle_PlayerCharacter_h_13_PROLOG
#define Dazle_Source_Dazle_PlayerCharacter_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_PlayerCharacter_h_16_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_PlayerCharacter_h_16_RPC_WRAPPERS \
	Dazle_Source_Dazle_PlayerCharacter_h_16_INCLASS \
	Dazle_Source_Dazle_PlayerCharacter_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_PlayerCharacter_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_PlayerCharacter_h_16_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_PlayerCharacter_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_PlayerCharacter_h_16_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_PlayerCharacter_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_PlayerCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
