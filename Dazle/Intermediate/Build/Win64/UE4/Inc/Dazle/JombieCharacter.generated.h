// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_JombieCharacter_generated_h
#error "JombieCharacter.generated.h already included, missing '#pragma once' in JombieCharacter.h"
#endif
#define DAZLE_JombieCharacter_generated_h

#define Dazle_Source_Dazle_JombieCharacter_h_11_RPC_WRAPPERS
#define Dazle_Source_Dazle_JombieCharacter_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define Dazle_Source_Dazle_JombieCharacter_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAJombieCharacter(); \
	friend struct Z_Construct_UClass_AJombieCharacter_Statics; \
public: \
	DECLARE_CLASS(AJombieCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(AJombieCharacter)


#define Dazle_Source_Dazle_JombieCharacter_h_11_INCLASS \
private: \
	static void StaticRegisterNativesAJombieCharacter(); \
	friend struct Z_Construct_UClass_AJombieCharacter_Statics; \
public: \
	DECLARE_CLASS(AJombieCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(AJombieCharacter)


#define Dazle_Source_Dazle_JombieCharacter_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AJombieCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AJombieCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AJombieCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AJombieCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AJombieCharacter(AJombieCharacter&&); \
	NO_API AJombieCharacter(const AJombieCharacter&); \
public:


#define Dazle_Source_Dazle_JombieCharacter_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AJombieCharacter(AJombieCharacter&&); \
	NO_API AJombieCharacter(const AJombieCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AJombieCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AJombieCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AJombieCharacter)


#define Dazle_Source_Dazle_JombieCharacter_h_11_PRIVATE_PROPERTY_OFFSET
#define Dazle_Source_Dazle_JombieCharacter_h_8_PROLOG
#define Dazle_Source_Dazle_JombieCharacter_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_JombieCharacter_h_11_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_JombieCharacter_h_11_RPC_WRAPPERS \
	Dazle_Source_Dazle_JombieCharacter_h_11_INCLASS \
	Dazle_Source_Dazle_JombieCharacter_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_JombieCharacter_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_JombieCharacter_h_11_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_JombieCharacter_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_JombieCharacter_h_11_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_JombieCharacter_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_JombieCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
