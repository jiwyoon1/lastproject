// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_IngameWidget_generated_h
#error "IngameWidget.generated.h already included, missing '#pragma once' in IngameWidget.h"
#endif
#define DAZLE_IngameWidget_generated_h

#define Dazle_Source_Dazle_IngameWidget_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execEmptyTestBtn) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->EmptyTestBtn(); \
		P_NATIVE_END; \
	}


#define Dazle_Source_Dazle_IngameWidget_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execEmptyTestBtn) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->EmptyTestBtn(); \
		P_NATIVE_END; \
	}


#define Dazle_Source_Dazle_IngameWidget_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUIngameWidget(); \
	friend struct Z_Construct_UClass_UIngameWidget_Statics; \
public: \
	DECLARE_CLASS(UIngameWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UIngameWidget)


#define Dazle_Source_Dazle_IngameWidget_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUIngameWidget(); \
	friend struct Z_Construct_UClass_UIngameWidget_Statics; \
public: \
	DECLARE_CLASS(UIngameWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(UIngameWidget)


#define Dazle_Source_Dazle_IngameWidget_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UIngameWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UIngameWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UIngameWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UIngameWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UIngameWidget(UIngameWidget&&); \
	NO_API UIngameWidget(const UIngameWidget&); \
public:


#define Dazle_Source_Dazle_IngameWidget_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UIngameWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UIngameWidget(UIngameWidget&&); \
	NO_API UIngameWidget(const UIngameWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UIngameWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UIngameWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UIngameWidget)


#define Dazle_Source_Dazle_IngameWidget_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__m_fValue() { return STRUCT_OFFSET(UIngameWidget, m_fValue); } \
	FORCEINLINE static uint32 __PPO__HpBar() { return STRUCT_OFFSET(UIngameWidget, HpBar); }


#define Dazle_Source_Dazle_IngameWidget_h_12_PROLOG
#define Dazle_Source_Dazle_IngameWidget_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_IngameWidget_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_IngameWidget_h_15_RPC_WRAPPERS \
	Dazle_Source_Dazle_IngameWidget_h_15_INCLASS \
	Dazle_Source_Dazle_IngameWidget_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_IngameWidget_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_IngameWidget_h_15_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_IngameWidget_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_IngameWidget_h_15_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_IngameWidget_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_IngameWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
