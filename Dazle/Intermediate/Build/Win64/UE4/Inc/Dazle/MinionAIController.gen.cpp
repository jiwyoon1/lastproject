// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Dazle/MinionAIController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMinionAIController() {}
// Cross Module References
	DAZLE_API UClass* Z_Construct_UClass_AMinionAIController_NoRegister();
	DAZLE_API UClass* Z_Construct_UClass_AMinionAIController();
	AIMODULE_API UClass* Z_Construct_UClass_AAIController();
	UPackage* Z_Construct_UPackage__Script_Dazle();
	AIMODULE_API UClass* Z_Construct_UClass_UBlackboardData_NoRegister();
	AIMODULE_API UClass* Z_Construct_UClass_UBehaviorTree_NoRegister();
// End Cross Module References
	void AMinionAIController::StaticRegisterNativesAMinionAIController()
	{
	}
	UClass* Z_Construct_UClass_AMinionAIController_NoRegister()
	{
		return AMinionAIController::StaticClass();
	}
	struct Z_Construct_UClass_AMinionAIController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_pBBData_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_pBBData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_pBTree_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_pBTree;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMinionAIController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AAIController,
		(UObject* (*)())Z_Construct_UPackage__Script_Dazle,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMinionAIController_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "MinionAIController.h" },
		{ "ModuleRelativePath", "MinionAIController.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMinionAIController_Statics::NewProp_pBBData_MetaData[] = {
		{ "ModuleRelativePath", "MinionAIController.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMinionAIController_Statics::NewProp_pBBData = { UE4CodeGen_Private::EPropertyClass::Object, "pBBData", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000000, 1, nullptr, STRUCT_OFFSET(AMinionAIController, pBBData), Z_Construct_UClass_UBlackboardData_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMinionAIController_Statics::NewProp_pBBData_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMinionAIController_Statics::NewProp_pBBData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMinionAIController_Statics::NewProp_pBTree_MetaData[] = {
		{ "ModuleRelativePath", "MinionAIController.h" },
		{ "ToolTip", "???\xe6\xbc\xb1??" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMinionAIController_Statics::NewProp_pBTree = { UE4CodeGen_Private::EPropertyClass::Object, "pBTree", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0040000000000000, 1, nullptr, STRUCT_OFFSET(AMinionAIController, pBTree), Z_Construct_UClass_UBehaviorTree_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMinionAIController_Statics::NewProp_pBTree_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMinionAIController_Statics::NewProp_pBTree_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMinionAIController_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMinionAIController_Statics::NewProp_pBBData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMinionAIController_Statics::NewProp_pBTree,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMinionAIController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMinionAIController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMinionAIController_Statics::ClassParams = {
		&AMinionAIController::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009002A0u,
		nullptr, 0,
		Z_Construct_UClass_AMinionAIController_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_AMinionAIController_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AMinionAIController_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AMinionAIController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMinionAIController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMinionAIController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMinionAIController, 1167960975);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMinionAIController(Z_Construct_UClass_AMinionAIController, &AMinionAIController::StaticClass, TEXT("/Script/Dazle"), TEXT("AMinionAIController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMinionAIController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
