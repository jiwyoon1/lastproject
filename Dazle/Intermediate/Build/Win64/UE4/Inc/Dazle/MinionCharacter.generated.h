// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DAZLE_MinionCharacter_generated_h
#error "MinionCharacter.generated.h already included, missing '#pragma once' in MinionCharacter.h"
#endif
#define DAZLE_MinionCharacter_generated_h

#define Dazle_Source_Dazle_MinionCharacter_h_13_RPC_WRAPPERS
#define Dazle_Source_Dazle_MinionCharacter_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Dazle_Source_Dazle_MinionCharacter_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMinionCharacter(); \
	friend struct Z_Construct_UClass_AMinionCharacter_Statics; \
public: \
	DECLARE_CLASS(AMinionCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(AMinionCharacter)


#define Dazle_Source_Dazle_MinionCharacter_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAMinionCharacter(); \
	friend struct Z_Construct_UClass_AMinionCharacter_Statics; \
public: \
	DECLARE_CLASS(AMinionCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Dazle"), NO_API) \
	DECLARE_SERIALIZER(AMinionCharacter)


#define Dazle_Source_Dazle_MinionCharacter_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMinionCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMinionCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMinionCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMinionCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMinionCharacter(AMinionCharacter&&); \
	NO_API AMinionCharacter(const AMinionCharacter&); \
public:


#define Dazle_Source_Dazle_MinionCharacter_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMinionCharacter(AMinionCharacter&&); \
	NO_API AMinionCharacter(const AMinionCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMinionCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMinionCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMinionCharacter)


#define Dazle_Source_Dazle_MinionCharacter_h_13_PRIVATE_PROPERTY_OFFSET
#define Dazle_Source_Dazle_MinionCharacter_h_10_PROLOG
#define Dazle_Source_Dazle_MinionCharacter_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_MinionCharacter_h_13_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_MinionCharacter_h_13_RPC_WRAPPERS \
	Dazle_Source_Dazle_MinionCharacter_h_13_INCLASS \
	Dazle_Source_Dazle_MinionCharacter_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dazle_Source_Dazle_MinionCharacter_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dazle_Source_Dazle_MinionCharacter_h_13_PRIVATE_PROPERTY_OFFSET \
	Dazle_Source_Dazle_MinionCharacter_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Dazle_Source_Dazle_MinionCharacter_h_13_INCLASS_NO_PURE_DECLS \
	Dazle_Source_Dazle_MinionCharacter_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dazle_Source_Dazle_MinionCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
